package example;

import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.sql.Timestamp;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class jdbc_pdf_report {
	public static void main(String[] args) throws Exception {

		/* Create Connection objects */
		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost", "lab", "cse301");
		Statement stmt = conn.createStatement();
		String s = "SELECT * from players";

		String timeStamp = new Timestamp(System.currentTimeMillis()).toString();
		String ts = timeStamp;
		ts = ts.replace(':', '_');
		ts = ts.replace('-', '_');
		ts = ts.replace(' ', '_');
		System.out.println(ts);
		String fileName = "Report_" + ts + ".pdf";

		/* Define the SQL query */
		ResultSet rs = stmt.executeQuery(s);

		/* Step-2: Initialize PDF documents - logical objects */
		Document mDocument = new Document();
		PdfWriter.getInstance(mDocument, new FileOutputStream(fileName));
		mDocument.open();

		ResultSetMetaData rsmd = rs.getMetaData();
		int columnsNumber = rsmd.getColumnCount();

		// we have four columns in our table
		PdfPTable mPdfPTable = new PdfPTable(columnsNumber);
		// create a cell object
		PdfPCell cell;

		for (int i = 1; i <= columnsNumber; i++) {
			String str = rsmd.getColumnName(i);
			cell = new PdfPCell(new Phrase(str));
			mPdfPTable.addCell(cell);

		}

		while (rs.next()) {
			for (int i = 1; i <= columnsNumber; i++) {
				String str = rs.getString(i);
				cell = new PdfPCell(new Phrase(str));
				mPdfPTable.addCell(cell);
			}
		}

		Paragraph parReportTitle = new Paragraph();
		parReportTitle.add("Report on " + s + "\n\n\n");
		parReportTitle.setAlignment(Element.ALIGN_CENTER);
		mDocument.add(parReportTitle);

		/* Attach report table to PDF */
		mDocument.add(mPdfPTable);
		mDocument.addCreationDate();

		Paragraph parReportDate = new Paragraph();
		parReportDate.add("\n\n\n Report generated on " + timeStamp);
		parReportDate.setAlignment(Element.ALIGN_RIGHT);
		mDocument.add(parReportDate);

		mDocument.close();

		/* Close all DB related objects */
		rs.close();
		stmt.close();
		conn.close();
	}

}