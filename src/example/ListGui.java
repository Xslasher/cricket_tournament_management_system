package example;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import utils.Tools;

import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class ListGui extends JFrame implements ListSelectionListener {

	private JPanel contentPane;
	ArrayList<String> al;
	JComboBox comboBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ListGui frame = new ListGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ListGui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		//String[] fontStyle = { "Normal", "Bold", "Italic", "Bold & Italic", "Big" };
		String[] arr = {"a","b","c","d","e","f"};
		final String[] arr2 = {"a","b","c","d","e","f","x","y","z"};
		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(85, 73, 145, 69);
		contentPane.add(scrollPane);

		final JList list = new JList();
		list.setListData(arr);
		scrollPane.setViewportView(list);
		//list.setVisibleRowCount(3);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				list.setListData(arr2);
				//comboBox = new JComboBox(arr2);
				//comboBox.addItem("new");
				comboBox = new JComboBox(arr2);
			}
		});
		btnAdd.setBounds(111, 179, 89, 23);
		contentPane.add(btnAdd);
		
		JButton btnRemove = new JButton("remove");
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				comboBox.removeAllItems();
			}
		});
		btnRemove.setBounds(221, 179, 89, 23);
		contentPane.add(btnRemove);
		
		comboBox = new JComboBox(arr);
		comboBox.setSelectedIndex(0);
		
		comboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				
				//comboBox.removeAllItems();
				
				//if(comboBox.size() != null)
				//comboBox.removeItemAt(1);
				
				Tools.pToast("itst func called");
			}
		});
		
		comboBox.setBounds(282, 72, 112, 20);
		contentPane.add(comboBox);

		list.addListSelectionListener(this);
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		// TODO Auto-generated method stub
//		JList lv = (JList) e.getSource();
//
//		String s;
//
//		int n = lv.getSelectedIndex();
//
//		s = lv.getSelectedValue().toString();
//		// s=e.getItem().toString();
//
//		Tools.pToast("" + n + s);
	}
}
