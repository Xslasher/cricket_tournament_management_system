package frame;

import java.awt.EventQueue;
import java.sql.ResultSet;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import database.oracle.OracleHandler;
import net.proteanit.sql.DbUtils;
import utils.DataUtils;
import utils.SqlHelper;
import utils.Tools;

public class PointTable extends JFrame {

	private JPanel contentPane;
	private JTable tableResult;
	private JTable tablePointTable;
	
	OracleHandler mOracleHandler = new OracleHandler();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PointTable frame = new PointTable();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PointTable() {
		
		setTitle(DataUtils.FRAME_TITLE_POINT_TALBE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 850, 490);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 56, 417, 385);
		contentPane.add(scrollPane);
		
		tableResult = new JTable();
		scrollPane.setViewportView(tableResult);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(437, 56, 387, 385);
		contentPane.add(scrollPane_1);
		
		tablePointTable = new JTable();
		scrollPane_1.setViewportView(tablePointTable);
		
		JLabel lblResult = new JLabel("Result");
		lblResult.setBounds(167, 22, 46, 14);
		contentPane.add(lblResult);
		
		JLabel lblPointTable = new JLabel("Point Table");
		lblPointTable.setBounds(587, 22, 72, 14);
		contentPane.add(lblPointTable);
		
		firstView();
	}

	private void firstView() {
		// TODO Auto-generated method stub
		
		setTablePoint(DataUtils.TABLE_POINT);
		setTableResult(DataUtils.TABLE_RESULT);
		
	}
	
	private void setTableResult(String tableName) {
		// TODO Auto-generated method stub
		
		if(tableName==null){
			System.err.println(tableName+" tableName is null in setTtable func");			
			return;
		}
		mOracleHandler.openDatabase();
		
		ResultSet rs = null;
		try {
			rs = mOracleHandler.runQuery(SqlHelper.getATable(tableName));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println(tableName+" runQuery failed");
			Tools.pToast(tableName+" runQuery failed");
			return;
		}
		
		tableResult.setModel(DbUtils.resultSetToTableModel(rs));
		mOracleHandler.close();
		
	}
	
	private void setTablePoint(String tableName) {
		// TODO Auto-generated method stub
		
		if(tableName==null){
			System.err.println(tableName+" tableName is null in setTtable func");			
			return;
		}
		mOracleHandler.openDatabase();
		
		ResultSet rs = null;
		try {
			rs = mOracleHandler.runQuery(SqlHelper.getATable(tableName));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println(tableName+" runQuery failed");
			Tools.pToast(tableName+" runQuery failed");
			return;
		}
		
		tablePointTable.setModel(DbUtils.resultSetToTableModel(rs));
		mOracleHandler.close();
		
	}
	

}
