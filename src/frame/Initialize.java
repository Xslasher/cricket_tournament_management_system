package frame;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import database.oracle.OracleHandler;
import utils.DataUtils;
import utils.SqlHelper;
import utils.Tools;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class Initialize extends JFrame {

	private JPanel contentPane;
	
	OracleHandler mOracleHandler;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Initialize frame = new Initialize();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Initialize() {
		setTitle(DataUtils.FRAME_TITLE_INITIALIZE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		mOracleHandler = new OracleHandler();
		
		JButton btnCreateTableAllcricteams = new JButton("Create table all_cric_teams");
		btnCreateTableAllcricteams.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				mOracleHandler.openDatabase();
				try {
					//mOracleHandler.runQuery(SqlHelper.createTableAllCricTeams());
					mOracleHandler.runUpdate(SqlHelper.createTable(DataUtils.TABLE_ALL_CRIC_TEAMS));
					
					Tools.pToast("Table Created");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Tools.pToast("Table not Created/may be exist");
				}
				mOracleHandler.close();
			}
		});
		btnCreateTableAllcricteams.setBounds(10, 11, 238, 23);
		contentPane.add(btnCreateTableAllcricteams);
		
		JButton btnCreateTableTournamentteams = new JButton("Create table tournament_teams");
		btnCreateTableTournamentteams.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				mOracleHandler.openDatabase();
				try {
					mOracleHandler.runUpdate(SqlHelper.createTable(DataUtils.TABLE_TOURNAMENT_TEAMS));
					Tools.pToast("Table Created");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Tools.pToast("Table not Created/may be exist");
				}
				mOracleHandler.close();
			}
		});
		btnCreateTableTournamentteams.setBounds(10, 32, 238, 23);
		contentPane.add(btnCreateTableTournamentteams);
		
		JButton btnCtSquadsPlayers = new JButton("ct squads, players");
		btnCtSquadsPlayers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				
				mOracleHandler.openDatabase();
				try {
					mOracleHandler.runSql(SqlHelper.createTable(DataUtils.TABLE_PLAYERS));
					mOracleHandler.runSql(SqlHelper.createTable(DataUtils.TABLE_SQUADS));
					
					Tools.pToast("Table Created");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Tools.pToast("Table not Created/may be exist");
				}
				
				mOracleHandler.close();
			}
		});
		btnCtSquadsPlayers.setBounds(10, 53, 238, 23);
		contentPane.add(btnCtSquadsPlayers);
		
		JButton btnNewButton = new JButton("Match odi t20");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				mOracleHandler.openDatabase();
				try {
					mOracleHandler.runSql(SqlHelper.createTable(DataUtils.TABLE_MATCH_ODI));
					mOracleHandler.runSql(SqlHelper.createTable(DataUtils.TABLE_MATCH_T20));
					System.out.println("Table Created");
					Tools.pToast("Table Created");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.err.println("Table not Created/may be exist");
					Tools.pToast("Table not Created/may be exist");
				}
				
				mOracleHandler.close();
			}
		});
		btnNewButton.setBounds(10, 74, 238, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("create view ");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				

				String sql = null;
				mOracleHandler.openDatabase();
				try {
					
					sql = SqlHelper.createViewMatchLt(DataUtils.VIEW_MATCH_ODI, DataUtils.TABLE_MATCH_ODI);
					//System.out.println(sql);
					mOracleHandler.runSql(sql);
						
					sql = SqlHelper.createViewMatchLt(DataUtils.VIEW_MATCH_T20, DataUtils.TABLE_MATCH_T20);
					mOracleHandler.runSql(sql);
					
					
					System.out.println("View Created");
					Tools.pToast("View Created");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.err.println("View not Created/may be exist");
					Tools.pToast("View not Created/may be exist");
				}
				
				mOracleHandler.close();
			}
		});
		btnNewButton_1.setBounds(10, 94, 238, 23);
		contentPane.add(btnNewButton_1);
		
		JButton btnCreateFinalSquad = new JButton("create final squad");
		btnCreateFinalSquad.setBounds(10, 114, 238, 23);
		contentPane.add(btnCreateFinalSquad);
		
		JButton btnCreateBatBowl = new JButton("create bat bowl table");
		btnCreateBatBowl.setBounds(10, 136, 238, 23);
		contentPane.add(btnCreateBatBowl);
		
		JButton btnNewButton_2 = new JButton("toss");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				mOracleHandler.openDatabase();
				try {
					mOracleHandler.runSql(SqlHelper.createTable(DataUtils.TABLE_TOSS));
					
					System.out.println("Table Created");
					Tools.pToast("Table Created");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.err.println("Table not Created/may be exist");
					Tools.pToast("Table not Created/may be exist");
				}
				
				mOracleHandler.close();
			}
		});
		btnNewButton_2.setBounds(10, 158, 238, 23);
		contentPane.add(btnNewButton_2);
		
		JButton btnCrTableInnings = new JButton("Cr table Innings");
		btnCrTableInnings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				createTable(DataUtils.TABLE_INNINGS);
			}
		});
		btnCrTableInnings.setBounds(10, 181, 238, 23);
		contentPane.add(btnCrTableInnings);
	}
	
	private void createTable(String tableName){
		mOracleHandler.openDatabase();
		try {
			mOracleHandler.runSql(SqlHelper.createTable(tableName));
			
			System.out.println("Table Created");
			Tools.pToast("Table Created");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println("Table not Created/may be exist");
			Tools.pToast("Table not Created/may be exist");
		}
		
		mOracleHandler.close();
	}
}
