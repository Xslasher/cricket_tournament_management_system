package frame;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import database.sqlite.SqliteAdapter;
import database.sqlite.SqliteHandler;
import model.PlayerB;
import net.proteanit.sql.DbUtils;
import utils.DataUtils;
import utils.RSHelper;
import utils.Tools;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.JScrollPane;

public class SqliteBrowser extends JFrame {

	private JPanel contentPane;
	private JTextField tfSql;
	private JTable table;
	
	private String sql;
	private ResultSet rs;
	
	private SqliteHandler mSqliteReader;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SqliteBrowser frame = new SqliteBrowser();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SqliteBrowser() {
		setTitle(DataUtils.FRAME_TITLE_SQLITE_BROWSER);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 632, 434);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		mSqliteReader = new SqliteHandler();
		mSqliteReader.openDatabase("cric.db");
		
		
		
		JLabel lblEnterQuery = new JLabel("Enter Query");
		lblEnterQuery.setBounds(10, 11, 73, 19);
		contentPane.add(lblEnterQuery);
		
		tfSql = new JTextField();
		tfSql.setText("select * from ");
		tfSql.setBounds(93, 8, 414, 22);
		contentPane.add(tfSql);
		tfSql.setColumns(10);
		
		JButton btnRun = new JButton("Run");
		btnRun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				sql = tfSql.getText().toString().replace(';', ' ').trim();
				//sql = "select * from all_cric_teams";
				//rs = SqliteAdapter.getResultSet(sql, "cric.db");
				try {
					rs = mSqliteReader.runQuery(sql);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				//setResultSetTable(rs);
				
				ArrayList<PlayerB> pl = RSHelper.getSquadB(rs);
				System.out.println(pl.toString());
				
//				for (int i = 0; i < pl.size(); i++) {
//					System.out.println(pl.get(i).toString());
//				}
			}

		
		});
		btnRun.setBounds(517, 7, 89, 23);
		contentPane.add(btnRun);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(74, 60, 482, 304);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
	}
	private void setResultSetTable(ResultSet rs) {
		// TODO Auto-generated method stub
		table.setModel(DbUtils.resultSetToTableModel(rs));
	}
}
