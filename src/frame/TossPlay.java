package frame;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import database.oracle.OracleHandler;
import utils.DataUtils;
import utils.PrefUtils;
import utils.RSHelper;
import utils.SqlHelper;
import utils.Tools;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

public class TossPlay extends JFrame {

	private JPanel contentPane;
	JLabel lblStatus;
	JTextArea taInnStatus;
	JLabel lblTossStatus;

	String team1Id = PrefUtils.getString(PrefUtils.TEAM1_ID, "");
	String team2Id = PrefUtils.getString(PrefUtils.TEAM2_ID, "");

	String team1Name = PrefUtils.getString(PrefUtils.TEAM1_NAME, "");
	String team2Name = PrefUtils.getString(PrefUtils.TEAM2_NAME, "");
	String team1Code = PrefUtils.getString(PrefUtils.TEAM1_CODE, "");
	String team2Code = PrefUtils.getString(PrefUtils.TEAM2_CODE, "");

	String matchId = PrefUtils.getString(PrefUtils.CURRENT_MATCH_ID, "");

	String selTeamId;
	String selTeamName;
	String selTeamCode;

	// String tossWinTeamId,tossLooseTeamId;

	OracleHandler mOracleHandler = new OracleHandler();
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TossPlay frame = new TossPlay();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TossPlay() {
		setTitle(DataUtils.FRAME_TITLE_TOSS);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 583, 424);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblMatchTitle = new JLabel("Match Title");
		lblMatchTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblMatchTitle.setBounds(187, 11, 179, 14);
		contentPane.add(lblMatchTitle);
		lblMatchTitle.setText(team1Name + " Vs " + team2Name);

		JComboBox cmbTeam = new JComboBox();
		cmbTeam.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				JComboBox cmb = (JComboBox) e.getSource();
				String s = (String) cmb.getSelectedItem();
				// Tools.pToast(s);
				selTeamName = s;

				if (selTeamName.equalsIgnoreCase(team1Name)) {
					selTeamId = team1Id;
					selTeamCode = team1Code;
				} else {
					selTeamId = team2Id;
					selTeamCode = team2Code;
				}

				Tools.pToast(selTeamId + " " + selTeamName);

			}
		});
		cmbTeam.addItem(team1Name);
		cmbTeam.addItem(team2Name);
		cmbTeam.setSelectedIndex(0);

		cmbTeam.setBounds(122, 91, 131, 20);
		contentPane.add(cmbTeam);

		JRadioButton rdbtnBat = new JRadioButton("Bat");
		rdbtnBat.setActionCommand("bat");
		buttonGroup.add(rdbtnBat);
		rdbtnBat.setBounds(329, 90, 53, 23);
		contentPane.add(rdbtnBat);

		JRadioButton rdbtnBowl = new JRadioButton("Bowl");
		rdbtnBowl.setActionCommand("bowl");
		buttonGroup.add(rdbtnBowl);
		rdbtnBowl.setBounds(384, 90, 73, 23);
		contentPane.add(rdbtnBowl);

		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (buttonGroup.getSelection() == null) {
					Tools.pToast("Select Bat or Bowl");
					return;
				}

				if (selTeamId == null || selTeamId.length() < 2) {
					Tools.pToast("Invalid Team");
					return;
				}

				String choose = buttonGroup.getSelection().getActionCommand().toString();
				// Tools.pToast(choose);

				String sqlTossWin = SqlHelper.updateTable(DataUtils.TABLE_TOSS, "toss_win", selTeamId, "match_id",
						matchId);
				String sqlChoose = SqlHelper.updateTable(DataUtils.TABLE_TOSS, "choose", choose, "match_id", matchId);

				mOracleHandler.openDatabase();

				try {
					mOracleHandler.runUpdate(sqlTossWin);
					mOracleHandler.runUpdate(sqlChoose);

				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.err.println("Toss update fallied");
					e1.printStackTrace();
				}

				mOracleHandler.close();

				fixInnings();
				refreshTask();
			}
		});
		btnUpdate.setBounds(223, 138, 89, 23);
		contentPane.add(btnUpdate);

		lblStatus = new JLabel("Status");
		lblStatus.setHorizontalAlignment(SwingConstants.CENTER);
		lblStatus.setBounds(124, 36, 308, 14);
		contentPane.add(lblStatus);

		JLabel lblTossWin = new JLabel("Toss Win");
		lblTossWin.setBounds(53, 94, 59, 14);
		contentPane.add(lblTossWin);

		JLabel lblChoose = new JLabel("Choose");
		lblChoose.setBounds(277, 94, 46, 14);
		contentPane.add(lblChoose);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(150, 194, 237, 96);
		contentPane.add(scrollPane);

		taInnStatus = new JTextArea();
		taInnStatus.setEditable(false);
		scrollPane.setViewportView(taInnStatus);

		JButton btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				refreshTask();
			}
		});
		btnRefresh.setBounds(223, 301, 89, 23);
		contentPane.add(btnRefresh);

		lblTossStatus = new JLabel("Toss status");
		lblTossStatus.setHorizontalAlignment(SwingConstants.TRAILING);
		lblTossStatus.setBounds(171, 361, 386, 14);
		contentPane.add(lblTossStatus);

		firstView();
	}

	protected void fixInnings() {
		// TODO Auto-generated method stub

		String tossWinTeamId, tossLooseTeamId;

		tossWinTeamId = getTossWinTeamId(matchId);
		if (team1Id.equalsIgnoreCase(tossWinTeamId)) {
			tossLooseTeamId = team2Id;
		} else {
			tossLooseTeamId = team1Id;
		}

		String choose = getChoose(matchId);

		String tossStatus = matchId + " toss win: " + tossWinTeamId + " toss loose: " + tossLooseTeamId + " Choose: "
				+ choose;
		lblTossStatus.setText(tossStatus);

		String bat1, bat2, bowl1, bowl2;

		bat1 = Tools.getTeamBatting(tossWinTeamId, tossLooseTeamId, choose, 1);
		bowl1 = Tools.getTeamBowling(tossWinTeamId, tossLooseTeamId, choose, 1);

		bat2 = Tools.getTeamBatting(tossWinTeamId, tossLooseTeamId, choose, 2);
		bowl2 = Tools.getTeamBowling(tossWinTeamId, tossLooseTeamId, choose, 2);

		
		 String inningsStatus = Tools.getInningsDetails(bat1, bowl1, bat2,
		 bowl2);
		 //Tools.pToast(inningsStatus);
		Tools.printMessage(inningsStatus);

		mOracleHandler.openDatabase();
		String sql = null;
		String tableName = DataUtils.TABLE_INNINGS;

		try {
			sql = SqlHelper.updateTable(tableName, "inn1_bat", bat1, "match_id", matchId);
			mOracleHandler.runUpdate(sql);

			sql = SqlHelper.updateTable(tableName, "inn1_bowl", bowl1, "match_id", matchId);
			mOracleHandler.runUpdate(sql);

			sql = SqlHelper.updateTable(tableName, "inn2_bat", bat2, "match_id", matchId);
			mOracleHandler.runUpdate(sql);

			sql = SqlHelper.updateTable(tableName, "inn2_bowl", bowl2, "match_id", matchId);
			mOracleHandler.runUpdate(sql);

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			// e1.printStackTrace();
			System.err.println("innings update failed");
		}

		mOracleHandler.close();
	}

	private String getTossStatus() {

		mOracleHandler.openDatabase();
		String sql = null;
		ResultSet rs = null;

		String tossWinTeamId = null, tossLooseTeamId = null, choose = null, tossWinTeamName = null;

		try {
			rs = mOracleHandler.runQuery(SqlHelper.getACell(DataUtils.TABLE_TOSS, "toss_win", "match_id", matchId));
			tossWinTeamId = RSHelper.getString(rs);
			System.out.println("Toss win team id: " + tossWinTeamId);
			System.out.println("match id: " + matchId);

			rs = mOracleHandler.runQuery(SqlHelper.getACell(DataUtils.TABLE_TOSS, "choose", "match_id", matchId));
			choose = RSHelper.getString(rs);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.err.println("Something wrong");
			e.printStackTrace();
		}

		if (tossWinTeamId.equalsIgnoreCase(team1Id)) {
			tossWinTeamName = team1Name;
			tossLooseTeamId = team2Id;
		} else if (tossWinTeamId.equalsIgnoreCase(team2Id)) {
			tossWinTeamName = team2Name;
			tossLooseTeamId = team1Id;
		}

		mOracleHandler.close();

		String s = tossWinTeamName + " won the toss and decided to " + choose + " first";

		// extra
//		if (choose.equalsIgnoreCase("bat")) {
//			PrefUtils.putString(PrefUtils.CURRENET_BATTING, tossWinTeamId);
//			PrefUtils.putString(PrefUtils.CURRENET_BOWLING, tossLooseTeamId);
//
//		} else if (choose.equalsIgnoreCase("bowl")) {
//			PrefUtils.putString(PrefUtils.CURRENET_BOWLING, tossWinTeamId);
//			PrefUtils.putString(PrefUtils.CURRENET_BATTING, tossLooseTeamId);
//		}

		return s;
	}

	private void firstView() {
		// TODO Auto-generated method stub

		try {
			String tossStatus = getTossStatus();
			lblStatus.setText(tossStatus);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	private void refreshTask() {

		try {
			String tossStatus = getTossStatus();
			lblStatus.setText(tossStatus);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		String inningsStatus = getInningsStatus();

		if (inningsStatus != null && inningsStatus.length() > 5) {
			taInnStatus.setText(inningsStatus);
		}

	}

	private String getInningsStatus() {
		// TODO Auto-generated method stub
		mOracleHandler.openDatabase();
		String sql = null;
		String tableName = DataUtils.TABLE_INNINGS;
		ResultSet rs = null;

		String bat1 = null, bat2 = null, bowl1 = null, bowl2 = null;

		try {
			sql = SqlHelper.getACell(tableName, "inn1_bat", "match_id", matchId);
			rs = mOracleHandler.runQuery(sql);
			bat1 = RSHelper.getString(rs);

			sql = SqlHelper.getACell(tableName, "inn1_bowl", "match_id", matchId);
			rs = mOracleHandler.runQuery(sql);
			bowl1 = RSHelper.getString(rs);

			sql = SqlHelper.getACell(tableName, "inn2_bat", "match_id", matchId);
			rs = mOracleHandler.runQuery(sql);
			bat2 = RSHelper.getString(rs);

			sql = SqlHelper.getACell(tableName, "inn2_bowl", "match_id", matchId);
			rs = mOracleHandler.runQuery(sql);
			bowl2 = RSHelper.getString(rs);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		mOracleHandler.close();

		String inningsStatus = "Inn1Bat: " + bat1 + "\nInn1Bowl: " + bowl1 + "\nInn2Bat: " + bat2 + "\nInn2Bowl: "
				+ bowl2;

		return inningsStatus;
	}

	private String getTossWinTeamId(String matchId) {

		mOracleHandler.openDatabase();
		String sql = SqlHelper.getACell(DataUtils.TABLE_TOSS, "toss_win", "match_id", matchId);
		ResultSet rs = null;

		try {
			rs = mOracleHandler.runQuery(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			System.err.println("getTossWinTeamId failed");
		}
		String s = RSHelper.getString(rs);

		mOracleHandler.close();

		return s;
	}

	private String getChoose(String matchId) {

		mOracleHandler.openDatabase();
		String sql = SqlHelper.getACell(DataUtils.TABLE_TOSS, "choose", "match_id", matchId);
		ResultSet rs = null;

		try {
			rs = mOracleHandler.runQuery(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			System.err.println("getChoose failed");
		}
		String s = RSHelper.getString(rs);

		mOracleHandler.close();

		return s;
	}

}
