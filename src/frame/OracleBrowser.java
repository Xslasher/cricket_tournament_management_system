package frame;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import database.oracle.OracleHandler;
import database.sqlite.SqliteHandler;
import net.proteanit.sql.DbUtils;
import utils.DataUtils;
import utils.RSHelper;
import utils.SqlHelper;
import utils.Tools;

public class OracleBrowser extends JFrame {

	private JPanel contentPane;
 JTextField tfSql;
	private JTable table;
	
	private String sql;
	//private ResultSet rs;
	
	private SqliteHandler mSqliteReader;
	private static OracleHandler mOracleHandler;
	private JLabel lblShow;
	private JComboBox comboBox;
	private JButton btnDrop;
	private JButton btnRefresh;
	
	private String tableName;
	ArrayList<String> allTables;
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OracleBrowser frame = new OracleBrowser();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public OracleBrowser() {
		setTitle(DataUtils.FRAME_TITLE_ORACLE_BROWSER);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 777, 505);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		mOracleHandler = new OracleHandler();
		//mOracleHandler.openDatabase();

		JLabel lblEnterQuery = new JLabel("Enter Query");
		lblEnterQuery.setBounds(10, 11, 73, 19);
		contentPane.add(lblEnterQuery);
		
		tfSql = new JTextField();
		tfSql.setText("select table_name from user_tables");
		tfSql.setBounds(93, 8, 559, 22);
		contentPane.add(tfSql);
		tfSql.setColumns(10);
		
		JButton btnRun = new JButton("Run");
		btnRun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				mOracleHandler.openDatabase();
				sql = tfSql.getText().toString().replace(';', ' ').trim();
				//sql = "select * from all_cric_teams";
				//rs = SqliteAdapter.getResultSet(sql, "cric.db");
				ResultSet rs =null;
				try {
					rs = mOracleHandler.runQuery(sql);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				setRSToTable(rs);
				mOracleHandler.close();
			}

		
		});
		btnRun.setBounds(662, 9, 89, 23);
		contentPane.add(btnRun);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 60, 741, 282);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		lblShow = new JLabel("Tables");
		lblShow.setBounds(93, 437, 46, 14);
		contentPane.add(lblShow);
		

		
		allTables = getAllUserTables();
		
		comboBox = new JComboBox(allTables.toArray());
		
//		for (int i = 0; i < al.size(); i++) {
//			comboBox.addItem(al.get(i));
//		}
		
		
		if(comboBox!=null && comboBox.getItemCount()>0){
			tableName = allTables.get(0);
			comboBox.setSelectedIndex(0);
			
			setTable(tableName);
			
		}
		
		comboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				
				JComboBox cmb = (JComboBox) e.getSource();
				String s = (String) cmb.getSelectedItem();
				//String s=e.getItem().toString();	//working wrong
				tableName=s;
				//Tools.pToast(s);
				if(tableName!=null){
					setTable(tableName);
				}
				

				
			}
		});
		comboBox.setBounds(171, 434, 202, 20);
		contentPane.add(comboBox);
		
		btnDrop = new JButton("Drop");
		btnDrop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(tableName==null){
					Tools.pToast(tableName+" enter valid table name");
					return;
				}
				mOracleHandler.openDatabase();
				try {
					mOracleHandler.runQuery(SqlHelper.dropTable(tableName));
					Tools.pToast(tableName + " table Dropped");
					
					refreshTask();
				} catch (SQLException e) {
					// TODO: handle exception
					//e.printStackTrace();
					Tools.pToast(tableName+" Drop Failed");
				}
				mOracleHandler.close();
				
				
			}
		});
		btnDrop.setBounds(429, 433, 89, 23);
		contentPane.add(btnDrop);
		
		btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				refreshTask();
			}
		});
		btnRefresh.setBounds(563, 433, 89, 23);
		contentPane.add(btnRefresh);
	}


	protected void refreshTask() {
		// TODO Auto-generated method stub
		comboBox.removeAllItems();
		allTables = getAllUserTables();
		//Tools.pToast(""+allTables.size());
		if(allTables.size() <= 0 ){
			Tools.pToast("No table");
			return;
		}
		
		
		//comboBox.removeAllItems();
		//comboBox = new JComboBox(allTables.toArray());
		for (int i = 0; i < allTables.size(); i++) {
			comboBox.addItem(allTables.get(i));
		}
	
		tableName = allTables.get(0);
		comboBox.setSelectedIndex(0);
		
		
		setTable(tableName);
		
		
	}

	private void setTable(String tableName) {
		// TODO Auto-generated method stub
		
		if(tableName==null){
			System.err.println(tableName+" tableName is null in setTtable func");			
			return;
		}
		mOracleHandler.openDatabase();
		
		ResultSet rs = null;
		try {
			rs = mOracleHandler.runQuery(SqlHelper.getATable(tableName));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println(tableName+" runQuery failed");
			Tools.pToast(tableName+" runQuery failed");
			return;
		}
		
		table.setModel(DbUtils.resultSetToTableModel(rs));
		mOracleHandler.close();
		
	}
	
	private void setRSToTable(ResultSet rs) {
		// TODO Auto-generated method stub
		table.setModel(DbUtils.resultSetToTableModel(rs));
	}
	
	private ArrayList<String> getAllUserTables(){
		ArrayList<String> al = new ArrayList<>();
		ResultSet rs = null;
		mOracleHandler.openDatabase();
		try {
			rs = mOracleHandler.runQuery(SqlHelper.getAllUserTables());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		al = RSHelper.getStringArrayList(rs);
		mOracleHandler.close();
		return al;
	}
	

	
	
}
