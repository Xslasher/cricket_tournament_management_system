package frame;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import database.oracle.OracleHandler;
import model.Match;
import utils.DataUtils;
import utils.PrefUtils;
import utils.RSHelper;
import utils.SqlHelper;
import utils.Tools;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.JTextField;

public class Home extends JFrame{

	private JPanel contentPane;
	
	JMenuItem mInputSeriesDetails;
	JMenuItem mInputTeams;
	private JMenu mnOption_1;
	private JMenuItem mntmDbPreferences;
	private JMenuItem mntmOracleBrowser;
	private JMenuItem mntmSqliteBrowser;

	
	SqliteBrowser mSqliteBrowser;
	OracleBrowser mOracleBrowser;
	OraclePreferences mOraclePreferences;
	private JMenuItem mntmInitialization;
	private JComboBox cmbMatch;
	private JButton btnRefresh;
	JButton btnEnter;
	
	ArrayList<String> allMatchesId = new ArrayList<>();
	
	OracleHandler mOracleHandler = new OracleHandler();
	private JMenuItem mntmDbAdmin;
	private JMenuItem mntmPrint;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Home frame = new Home();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Home() {
		setTitle(DataUtils.FRAME_TITLE_HOME);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 713, 501);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		mnOption_1 = new JMenu("Option");
		menuBar.add(mnOption_1);
		
		mntmSqliteBrowser = new JMenuItem("Sqlite Browser");
		mntmSqliteBrowser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				invisibleAll();
				
				if(mSqliteBrowser == null || !mSqliteBrowser.isVisible()){
					mSqliteBrowser = new SqliteBrowser();
					mSqliteBrowser.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
					mSqliteBrowser.setVisible(true);
				}else{
					//not null case
					
				}
				
			}

		
		});
		mnOption_1.add(mntmSqliteBrowser);
		
		mntmOracleBrowser = new JMenuItem("Oracle Browser");
		mntmOracleBrowser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				invisibleAll();
				
				if(mOracleBrowser == null || !mOracleBrowser.isVisible()){
					mOracleBrowser = new OracleBrowser();
					mOracleBrowser.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
					mOracleBrowser.setVisible(true);
				}else{
					//not null case
				}
				
			}
		});
		mnOption_1.add(mntmOracleBrowser);
		
		mntmDbPreferences = new JMenuItem("Oracle Preferences");
		mntmDbPreferences.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				invisibleAll();
				
				if(mOraclePreferences == null || !mOraclePreferences.isVisible()){
					mOraclePreferences = new OraclePreferences();
					mOraclePreferences.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
					mOraclePreferences.setVisible(true);
				}
				
			}
		});
		mnOption_1.add(mntmDbPreferences);
		
		JMenuItem mntmAllInternationalTeams = new JMenuItem("See All Teams");
		mntmAllInternationalTeams.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				SeeAllTeams frame = new SeeAllTeams();
				frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame.setVisible(true);
			}
		});
		mnOption_1.add(mntmAllInternationalTeams);
		
		mntmInitialization = new JMenuItem("Initialization");
		mntmInitialization.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Initialize frame = new Initialize();
				frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame.setVisible(true);
				
			}
		});
		mnOption_1.add(mntmInitialization);
		
		mntmDbAdmin = new JMenuItem("DB Admin");
		mntmDbAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DBAdmin frame = new DBAdmin();
				frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame.setVisible(true);
			}
		});
		mnOption_1.add(mntmDbAdmin);
		
		mntmPrint = new JMenuItem("Print Report");
		mntmPrint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ReportGenarator frame = new ReportGenarator();
				frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame.setVisible(true);
			
				
			}
		});
		mnOption_1.add(mntmPrint);
		
		JMenu mnOption = new JMenu("Tournament");
		menuBar.add(mnOption);
		
	
		
		mInputSeriesDetails = new JMenuItem("Tournament Details");
		mInputSeriesDetails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				TournamentDetails frame = new TournamentDetails();
				frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame.setVisible(true);
			}
		});
		mnOption.add(mInputSeriesDetails);
		
		mInputTeams = new JMenuItem("Team Details");
		mInputTeams.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				TeamDetails frame = new TeamDetails();
				frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame.setVisible(true);
			}
		});
		mnOption.add(mInputTeams);
		
		JMenu mnMatch = new JMenu("Match");
		menuBar.add(mnMatch);
		
		JMenuItem mntmFixMatch = new JMenuItem("Fix Match");
		mntmFixMatch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MatchFix frame = new MatchFix();
				frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame.setVisible(true);
			}
		});
		mnMatch.add(mntmFixMatch);
		
		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		JMenuItem mntmInstruction = new JMenuItem("Instruction");
		mntmInstruction.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Tools.printMessage("See the Doc File", "Instruction");
				
			}
		});
		mnHelp.add(mntmInstruction);
		
		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String aboutMsg = "Cricket Tournament Management"
						+ "\nVersion 0.1"
						+ "\nXplo";
				
				Tools.printMessage(aboutMsg, "About");
			}
		});
		mnHelp.add(mntmAbout);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		cmbMatch = new JComboBox();
		
		allMatchesId = getAllMatchesId();
		
		for (int i = 0; i < allMatchesId.size(); i++) {
			cmbMatch.addItem(allMatchesId.get(i));
		}
		
		cmbMatch.setBounds(203, 195, 181, 20);
		contentPane.add(cmbMatch);
		
		JLabel lblMatch = new JLabel("Match");
		lblMatch.setBounds(118, 198, 46, 14);
		contentPane.add(lblMatch);
		
		btnEnter = new JButton("Enter");
		
		btnEnter.setEnabled(true);
		
		btnEnter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				String matchId=cmbMatch.getSelectedItem().toString();
				Tools.pToast(matchId);
				if(matchId==null || matchId.length()<4){
					return;
				}
				
				saveCurrentMatchInfo(matchId);
				
				MatchPlay frame = new MatchPlay();
				frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame.setVisible(true);
			}
		});
		btnEnter.setBounds(441, 194, 89, 23);
		contentPane.add(btnEnter);
		
		btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				allMatchesId = getAllMatchesId();
				
				cmbMatch.removeAllItems();
				for (int i = 0; i < allMatchesId.size(); i++) {
					cmbMatch.addItem(allMatchesId.get(i));
				}
				
			}
		});
		btnRefresh.setBounds(10, 408, 89, 23);
		contentPane.add(btnRefresh);

		
	}

	protected void saveCurrentMatchInfo(String matchId) {
		// TODO Auto-generated method stub
		
		mOracleHandler.openDatabase();
		String sql = null;
		ResultSet rs= null;
		
		
		
		try {
			rs=mOracleHandler.runQuery(SqlHelper.getAMatchById(matchId));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		String matchType = Tools.getMatchTypeFromMatchId(matchId);
		Match mMatch = RSHelper.getAMatch(rs, matchType);
		
		
		
		String team1Id, team2Id, team1Name = null, team2Name = null;
		String team1Code = null, team2Code = null;
		
		team1Id = mMatch.getTeam1();
		team2Id = mMatch.getTeam2();
		
		try {
			rs = mOracleHandler.runQuery(SqlHelper.getTeamNameById(team1Id));
			team1Name = RSHelper.getString(rs);
			rs = mOracleHandler.runQuery(SqlHelper.getTeamNameById(team2Id));
			team2Name = RSHelper.getString(rs);
			
			rs = mOracleHandler.runQuery(SqlHelper.getTeamCodeById(team1Id));
			team1Code = RSHelper.getString(rs);
			rs = mOracleHandler.runQuery(SqlHelper.getTeamCodeById(team2Id));
			team2Code = RSHelper.getString(rs);
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.err.println("saveCurrentMatchInfo failed");
			e.printStackTrace();
			
		}
		
		
		
		
		
		PrefUtils.putString(PrefUtils.CURRENT_MATCH_ID, matchId);
		//PrefUtils.putString(PrefUtils.CURRENT_MATCH_OVER, 00);
		PrefUtils.putString(PrefUtils.TEAM1_ID, team1Id);
		PrefUtils.putString(PrefUtils.TEAM2_ID, team2Id);
		PrefUtils.putString(PrefUtils.TEAM1_NAME, team1Name);
		PrefUtils.putString(PrefUtils.TEAM2_NAME, team2Name);
		PrefUtils.putString(PrefUtils.TEAM1_CODE, team1Code);
		PrefUtils.putString(PrefUtils.TEAM2_CODE, team2Code);
		
		mOracleHandler.close();
		
	}

	private ArrayList<String> getAllMatchesId() {
		// TODO Auto-generated method stub
		
		 ArrayList<String> al = new ArrayList<>();
		 
		 String tableName=null;
		 
		 String matchType = PrefUtils.getString(PrefUtils.TOURNAMENT_MATCH_TYPE, "");
		 tableName = Tools.getTableNameFromMatchType(matchType);
		 
		 String sql = SqlHelper.getACol(tableName, "match_id");
		 ResultSet rs= null;
		 
		 mOracleHandler.openDatabase();
		 try {
			rs=mOracleHandler.runQuery(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		 al = RSHelper.getStringArrayList(rs);
		 
		 mOracleHandler.close();
		
		return al;
	}

	protected void invisibleAll() {
		// TODO Auto-generated method stub
		if(mOracleBrowser !=null && mOracleBrowser.isVisible()){
			mOracleBrowser.setVisible(false);
		}
		
		if(mSqliteBrowser !=null && mSqliteBrowser.isVisible()){
			mSqliteBrowser.setVisible(false);
		}
		
		if(mOraclePreferences !=null && mOraclePreferences.isVisible()){
			mOraclePreferences.setVisible(false);
		}
		
	}

	private void setInvisibleOther(ActionEvent e) {
		// TODO Auto-generated method stub
		//Tools.printMessage(e.getActionCommand());
	}
}
