package frame;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import database.oracle.OracleHandler;
import model.TournamentTeam;
import net.proteanit.sql.DbUtils;
import utils.DataUtils;
import utils.PrefUtils;
import utils.SqlHelper;
import utils.Tools;

import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import java.awt.event.ItemListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.RunnableScheduledFuture;
import java.awt.event.ItemEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class FinalSquad extends JFrame {

	private JPanel contentPane;
	private JTable tableTeamSquad;
	private JTable tableFinalSquad;
	
	
	String team1Id = PrefUtils.getString(PrefUtils.TEAM1_ID, "");
	String team2Id = PrefUtils.getString(PrefUtils.TEAM2_ID, "");
	String team1Name = PrefUtils.getString(PrefUtils.TEAM1_NAME, "");
	String team2Name = PrefUtils.getString(PrefUtils.TEAM2_NAME, "");
	String team1Code = PrefUtils.getString(PrefUtils.TEAM1_CODE, "");
	String team2Code = PrefUtils.getString(PrefUtils.TEAM2_CODE, "");
	String currentMatchId = PrefUtils.getString(PrefUtils.CURRENT_MATCH_ID, "");
	
	
	String selTeamId;
	String selTeamName;
	String selTeamCode;
	
	String selPlayerIdToAdd;
	String selPlayerIdToRemove;
	
	JComboBox cmbTeam;
	
	OracleHandler mOracleHandler = new OracleHandler();
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FinalSquad frame = new FinalSquad();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FinalSquad() {
		setTitle(DataUtils.FRAME_TITLE_FINAL_SQUAD);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 739, 475);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 73, 354, 286);
		contentPane.add(scrollPane);
		
		tableTeamSquad = new JTable();
		tableTeamSquad.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				int row = tableTeamSquad.getSelectedRow();
				selPlayerIdToAdd = tableTeamSquad.getModel().getValueAt(row, 0).toString();
				Tools.pToast(selPlayerIdToAdd);
				
//				String tableName = DataUtils.TABLE_SQUAD_PREFIX+selTeamCode;
//				addToFinalSquad(tableName,selPlayerIdToAdd);
			}
		});
		scrollPane.setViewportView(tableTeamSquad);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(374, 73, 339, 286);
		contentPane.add(scrollPane_1);
		
		tableFinalSquad = new JTable();
		tableFinalSquad.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				int row = tableFinalSquad.getSelectedRow();
				selPlayerIdToRemove = tableFinalSquad.getModel().getValueAt(row, 0).toString();
				Tools.pToast(selPlayerIdToRemove);
			}
		});
		scrollPane_1.setViewportView(tableFinalSquad);
		
		cmbTeam = new JComboBox();
		cmbTeam.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				
				JComboBox cmb = (JComboBox) e.getSource();
				String s = (String) cmb.getSelectedItem();
				//Tools.pToast(s);
				selTeamName=s;
				
				if(selTeamName.equalsIgnoreCase(team1Name)){
					selTeamId = team1Id;
					selTeamCode = team1Code;
				}else{
					selTeamId = team2Id;
					selTeamCode = team2Code;
				}
				
				Tools.pToast(selTeamId + " " +selTeamName);
				
				setTableTeamSquadByTeamId(selTeamId);
				setTableFinalSquadByTeamId(selTeamId);
				
			}
		});
		
		cmbTeam.addItem(team1Name);
		cmbTeam.addItem(team2Name);
		cmbTeam.setSelectedIndex(0);
		
		cmbTeam.setBounds(295, 11, 152, 20);
		contentPane.add(cmbTeam);
		
		
		
		JLabel lblTeam = new JLabel("Team");
		lblTeam.setBounds(223, 14, 46, 14);
		contentPane.add(lblTeam);
		
		JButton btnAddToSquad = new JButton("Add To Squad");
		btnAddToSquad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String tableName = DataUtils.TABLE_FINAL_SQUAD_PREFIX+ currentMatchId;
				addToFinalSquad(tableName,selPlayerIdToAdd,selTeamId);
				setTableFinalSquadByTeamId(selTeamId);
			}
		});
		btnAddToSquad.setBounds(109, 370, 119, 23);
		contentPane.add(btnAddToSquad);
		
		JButton btnRemove = new JButton("Remove");
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String tableName = DataUtils.TABLE_FINAL_SQUAD_PREFIX+ currentMatchId;
				removeFromFinalSquad(tableName,selPlayerIdToRemove);
				setTableFinalSquadByTeamId(selTeamId);
			}
		});
		btnRemove.setBounds(453, 370, 89, 23);
		contentPane.add(btnRemove);
		
		JButton btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				setTableFinalSquadByTeamId(selTeamId);
			}
		});
		btnRefresh.setBounds(316, 408, 89, 23);
		contentPane.add(btnRefresh);
		
		JLabel lblMatchinfo = new JLabel("matchInfo");
		lblMatchinfo.setHorizontalAlignment(SwingConstants.TRAILING);
		lblMatchinfo.setBounds(453, 412, 260, 14);
		contentPane.add(lblMatchinfo);
		lblMatchinfo.setText(Tools.getMatchDetailsFromPref());
		
		
		
		JLabel lblTeam_1 = new JLabel("Team");
		lblTeam_1.setBounds(156, 49, 46, 14);
		contentPane.add(lblTeam_1);
		
		JLabel lblFinalSquad = new JLabel("Final Squad");
		lblFinalSquad.setBounds(530, 49, 67, 14);
		contentPane.add(lblFinalSquad);
		
		JButton btnRemoveAll = new JButton("Remove All");
		btnRemoveAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String tableName = DataUtils.TABLE_FINAL_SQUAD_PREFIX+ currentMatchId;

				mOracleHandler.openDatabase();
				
				if(mOracleHandler.isTableExist(tableName)){
					try {
						//mOracleHandler.runSql(SqlHelper.deleteAllTuples(tableName));
						mOracleHandler.runSql(SqlHelper.deleteTuple(tableName,"team_id", selTeamId));
						
						//Tools.pToast(tableName + "Created");
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						//System.err.println(tableName + "Not Created");
						e1.printStackTrace();
					}
				}
				
				mOracleHandler.close();
				
				setTableFinalSquadByTeamId(selTeamId);
			}
		});
		btnRemoveAll.setBounds(552, 370, 105, 23);
		contentPane.add(btnRemoveAll);
		
		JButton button = new JButton("+");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				String tableName = DataUtils.TABLE_FINAL_SQUAD_PREFIX+currentMatchId;
				
				mOracleHandler.openDatabase();
		
				
				if(mOracleHandler.isTableExist(tableName)){
					System.out.println(tableName + " exist");
				}else{
					//System.out.println(tableName + " not exist");
					
					try {
						mOracleHandler.runSql(SqlHelper.createTable(tableName));
						//Tools.pToast(tableName + "Created");
						System.out.println(tableName + "Created");
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						System.err.println(tableName + "Not Created");
						e1.printStackTrace();
					}
				}
					
				mOracleHandler.close();
			}
		});
		button.setBounds(624, 10, 89, 23);
		contentPane.add(button);
		
		firstView();
	}
	
	
	protected void removeFromFinalSquad(String tableName, String playerId) {
		// TODO Auto-generated method stub
		
		mOracleHandler.openDatabase();
		
		
		try {
			mOracleHandler.runUpdate(SqlHelper.deleteTuple(tableName, "player_id", playerId));
			Tools.pToast("added");
			System.out.println("Player added");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.err.println("Player Not added");
		}
		
		mOracleHandler.close();
	}

	protected void addToFinalSquad(String tableName, String playerId, String teamId) {
		// TODO Auto-generated method stub
		mOracleHandler.openDatabase();
		String[] arg = {playerId,teamId};
		String sql =SqlHelper.insertValueByPs(tableName, 2);
		System.out.println(sql);
		try {
			mOracleHandler.runUpdate(sql,arg);
			Tools.pToast("added");
			System.out.println("Player added");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.err.println("Player Not added");
		}
		
		mOracleHandler.close();
		
	}

	private void firstView() {
		// TODO Auto-generated method stub
		
		//setTableByTeamId(selTeamId);
		
	}

	private void setTableTeamSquadByTeamId(String teamId) {
		// TODO Auto-generated method stub

		
		mOracleHandler.openDatabase();
		if (teamId == null) {
			System.err.println(teamId + " team is null in setTtableByTeam func");
			return;
		}

		ResultSet rs = null;
		try {
			rs = mOracleHandler.runQuery(SqlHelper.getSquadsByTeam(teamId, "player_id"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println(teamId + " runQuery failed");
			Tools.pToast(teamId + " runQuery failed");
			return;
		}

		if(rs!=null){
			tableTeamSquad.setModel(DbUtils.resultSetToTableModel(rs));
		}
		

		mOracleHandler.close();

	}
	
	private void setTableFinalSquadByTeamId(String teamId) {
		// TODO Auto-generated method stub

		
		mOracleHandler.openDatabase();
		if (teamId == null) {
			System.err.println(teamId + " team is null in setTtableByTeam func");
			return;
		}

		String tableName = DataUtils.TABLE_FINAL_SQUAD_PREFIX + currentMatchId;
		ResultSet rs = null;
		try {
			rs = mOracleHandler.runQuery(SqlHelper.getFinalSquadByTeam(tableName, teamId, "team_id"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println(teamId + " runQuery failed");
			Tools.pToast(teamId + " runQuery failed");
			return;
		}

		if(rs!=null){
			tableFinalSquad.setModel(DbUtils.resultSetToTableModel(rs));
		}
		

		mOracleHandler.close();

	}
}
