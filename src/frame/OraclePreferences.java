package frame;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import utils.DataUtils;
import utils.PrefUtils;
import utils.Toast;

public class OraclePreferences extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JTextField tfUserName;
	private JPasswordField pfPassword;
	private JTextField tfHost;
	private JTextField tfPort;
	private JTextField tfSid;

	private JButton btnUpdate;
	private JButton btnCancel;
	private JButton btnReset;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OraclePreferences frame = new OraclePreferences();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public OraclePreferences() {
		setTitle(DataUtils.FRAME_TITLE_ORACLE_PREF);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblUsername = new JLabel("UserName");
		lblUsername.setBounds(10, 11, 87, 14);
		contentPane.add(lblUsername);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(10, 36, 87, 14);
		contentPane.add(lblPassword);

		JLabel lblNewLabel = new JLabel("Host");
		lblNewLabel.setBounds(10, 61, 87, 14);
		contentPane.add(lblNewLabel);

		JLabel lblPort = new JLabel("Port");
		lblPort.setBounds(10, 86, 87, 14);
		contentPane.add(lblPort);

		JLabel lblSid = new JLabel("Sid");
		lblSid.setBounds(10, 109, 87, 14);
		contentPane.add(lblSid);

		tfUserName = new JTextField();
		tfUserName.setToolTipText("User Name");
		tfUserName.setBounds(107, 8, 86, 20);
		contentPane.add(tfUserName);
		tfUserName.setColumns(10);

		pfPassword = new JPasswordField();
		pfPassword.setToolTipText("Password");
		pfPassword.setBounds(107, 33, 86, 20);
		contentPane.add(pfPassword);

		tfHost = new JTextField();
		tfHost.setToolTipText("Host");
		tfHost.setBounds(107, 58, 86, 20);
		contentPane.add(tfHost);
		tfHost.setColumns(10);

		tfPort = new JTextField();
		tfPort.setToolTipText("Port");
		tfPort.setBounds(107, 83, 86, 20);
		contentPane.add(tfPort);
		tfPort.setColumns(10);

		tfSid = new JTextField();
		tfSid.setToolTipText("Sid");
		tfSid.setBounds(107, 106, 86, 20);
		contentPane.add(tfSid);
		tfSid.setColumns(10);

		 tfUserName.setText(PrefUtils.getString(PrefUtils.USER_NAME,
		 DataUtils.DEFAULT_USER_NAME));
		 pfPassword.setText(PrefUtils.getString(PrefUtils.USER_PASS,
		 DataUtils.DEFAULT_USER_PASS));
		 tfHost.setText(PrefUtils.getString(PrefUtils.HOST,
		 DataUtils.DEFAULT_HOST));
		 tfPort.setText("" + PrefUtils.getString(PrefUtils.PORT,
		 DataUtils.DEFAULT_PORT));
		 tfSid.setText(PrefUtils.getString(PrefUtils.SID,
		 DataUtils.DEFAULT_SID));
		//
		 btnUpdate = new JButton("Update");
		 btnUpdate.setBounds(67, 185, 89, 23);
		 contentPane.add(btnUpdate);
		
		 btnReset = new JButton("Reset");
		 btnReset.setBounds(166, 185, 89, 23);
		 contentPane.add(btnReset);
		
		 btnCancel = new JButton("Cancel");
		 btnCancel.setBounds(277, 185, 89, 23);
		 contentPane.add(btnCancel);
		
		 btnUpdate.addActionListener(this);
		 btnCancel.addActionListener(this);
		 btnReset.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

		String s = e.getActionCommand();
		// JOptionPane.showMessageDialog(null, s);

		if (e.getSource() == btnUpdate) {
			// setVisible(false);
			// dispose();

			String title = "Update";
			String message = "Are you sure to Update Preferences?";

			int res = JOptionPane.showConfirmDialog(OraclePreferences.this, message, title, JOptionPane.YES_NO_OPTION);
			// JOptionPane.showMessageDialog(null, res);

			if (res == 0) {
				String userName = tfUserName.getText().toString();
				String pass = pfPassword.getText().toString();
				String host = tfHost.getText().toString();
				String port = tfPort.getText().toString();
				String sid = tfSid.getText().toString();

				PrefUtils.putString(PrefUtils.USER_NAME, userName);
				PrefUtils.putString(PrefUtils.USER_PASS, pass);
				PrefUtils.putString(PrefUtils.HOST, host);
				PrefUtils.putString(PrefUtils.PORT, port);
				PrefUtils.putString(PrefUtils.SID, sid);
				JOptionPane.showMessageDialog(null, "successfully updated");

				// Toast toast = new Toast("Successfully Reset",3000);
				// toast.setVisible(true);

				setVisible(false);
				dispose();

			} else {

			}

		} else if (e.getSource() == btnCancel) {
			setVisible(false);
			dispose();
		} else if (e.getSource() == btnReset) {

			String title = "Reset?";
			String message = "Are you sure you want to Reset?";

			int res = JOptionPane.showConfirmDialog(OraclePreferences.this, message, title, JOptionPane.YES_NO_OPTION);
			// JOptionPane.showMessageDialog(null, res);

			if (res == 0) {

				PrefUtils.putString(PrefUtils.USER_NAME, DataUtils.DEFAULT_USER_NAME);
				PrefUtils.putString(PrefUtils.USER_PASS, DataUtils.DEFAULT_USER_PASS);
				PrefUtils.putString(PrefUtils.HOST, DataUtils.DEFAULT_HOST);
				PrefUtils.putString(PrefUtils.PORT, DataUtils.DEFAULT_PORT);
				PrefUtils.putString(PrefUtils.SID, DataUtils.DEFAULT_SID);

				tfUserName.setText(DataUtils.DEFAULT_USER_NAME);
				pfPassword.setText(DataUtils.DEFAULT_USER_PASS);
				tfHost.setText(DataUtils.DEFAULT_HOST);
				tfPort.setText(DataUtils.DEFAULT_PORT);
				tfSid.setText(DataUtils.DEFAULT_SID);

				// JOptionPane.showMessageDialog(null, "Successfully Reset");

				Toast toast = new Toast("Successfully Reset", 3000);
				toast.setVisible(true);

			} else {

			}

		}

	}
}
