package frame;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import database.oracle.OracleHandler;
import database.sqlite.SqliteHandler;
import model.Player;
import model.TournamentTeam;
import net.proteanit.sql.DbUtils;
import utils.DataUtils;
import utils.RSHelper;
import utils.SqlHelper;
import utils.Tools;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class TeamDetails extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField tfCoach;
	private JTextField tfManager;
	private JTextField tfCaptain;
	private JTextField tfViceCap;

	private JTextField tfFirstName;
	private JTextField tfLastName;
	JCheckBox chbBat, chbBowl, chbWk;

	JComboBox cmbTeam;
	JComboBox cmbPlayer;

	int cmbTeamPos = 0;
	// int cmbViceCaptainPos = 0;
	int cmbPlayerPos = 0;
	
	String selPlayerIdToRemove;
	String currentTeamId;

	ArrayList<TournamentTeam> tournamentTeams = new ArrayList<>();
	ArrayList<String> teamsName = new ArrayList<>();

	OracleHandler mOracleHandler = new OracleHandler();
	private JTextField tfIDA;
	private JTextField tfIDB;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TeamDetails frame = new TeamDetails();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TeamDetails() {
		setTitle(DataUtils.FRAME_TITLE_TEAM_DETAILS);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 830, 497);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Team");
		lblNewLabel.setBounds(269, 11, 46, 14);
		contentPane.add(lblNewLabel);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 129, 794, 250);
		contentPane.add(scrollPane);

		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				int row = table.getSelectedRow();
				selPlayerIdToRemove = table.getModel().getValueAt(row, 0).toString();
				Tools.pToast(selPlayerIdToRemove);
			}
		});
		scrollPane.setViewportView(table);

		// String[] teams = { "Bangladesh", "India", "Pakistan", "Srilanka" };

		cmbTeam = new JComboBox();
		cmbTeam.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				JComboBox cmb = (JComboBox) e.getSource();
				// String s = (String) cmb.getSelectedItem();

				cmbTeamPos = cmb.getSelectedIndex();
				// Tools.pToast(tournamentTeams.get(cmbTeamPos).toString());
				// System.out.println("see: " +
				// tournamentTeams.get(cmbTeamPos).toString());
				setFieldByTeam();

				// System.out.println("see: " +
				// tournamentTeams.get(cmbTeamPos).toString());
				setTableByTeam(tournamentTeams.get(cmbTeamPos));
				

			}
		});

		refreshTournamentTeamsArrayList();

		JPanel panel = new JPanel();
		panel.setBounds(10, 39, 794, 79);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblCaptain = new JLabel("Coach");
		lblCaptain.setBounds(0, 11, 46, 14);
		panel.add(lblCaptain);

		JLabel lblNewLabel_2 = new JLabel("F. Name");
		lblNewLabel_2.setBounds(160, 39, 54, 14);
		panel.add(lblNewLabel_2);

		tfCoach = new JTextField();
		tfCoach.setBounds(56, 8, 94, 20);
		panel.add(tfCoach);
		tfCoach.setText("x");
		tfCoach.setColumns(10);

		tfFirstName = new JTextField();
		tfFirstName.setBounds(224, 36, 112, 20);
		panel.add(tfFirstName);
		tfFirstName.setColumns(10);

		JLabel lblManager = new JLabel("Manager");
		lblManager.setBounds(160, 11, 56, 14);
		panel.add(lblManager);

		tfManager = new JTextField();
		tfManager.setBounds(226, 8, 110, 20);
		panel.add(tfManager);
		tfManager.setText("x");
		tfManager.setColumns(10);

		JLabel lblCaptain_2 = new JLabel("Captain");
		lblCaptain_2.setBounds(363, 11, 46, 14);
		panel.add(lblCaptain_2);

		tfCaptain = new JTextField();
		tfCaptain.setBounds(419, 8, 103, 20);
		panel.add(tfCaptain);
		tfCaptain.setColumns(10);

		JLabel lblViceCaptain = new JLabel("V. Captain");
		lblViceCaptain.setBounds(532, 11, 60, 14);
		panel.add(lblViceCaptain);

		tfViceCap = new JTextField();
		tfViceCap.setBounds(602, 8, 104, 20);
		panel.add(tfViceCap);
		tfViceCap.setColumns(10);

		JLabel lblLastName = new JLabel("L. Name");
		lblLastName.setBounds(361, 39, 52, 14);
		panel.add(lblLastName);

		tfLastName = new JTextField();
		tfLastName.setBounds(419, 36, 103, 20);
		panel.add(tfLastName);
		tfLastName.setColumns(10);

		JButton btnAddPlayer = new JButton("Add Player");
		btnAddPlayer.setBounds(699, 35, 95, 23);
		panel.add(btnAddPlayer);

		chbWk = new JCheckBox("Wk");
		chbWk.setBounds(637, 35, 56, 23);
		panel.add(chbWk);

		chbBowl = new JCheckBox("Bowl");
		chbBowl.setBounds(576, 35, 59, 23);
		panel.add(chbBowl);

		chbBat = new JCheckBox("Bat");
		chbBat.setBounds(528, 35, 46, 23);
		panel.add(chbBat);

		JLabel lblId = new JLabel("ID");
		lblId.setBounds(0, 39, 23, 14);
		panel.add(lblId);

		tfIDA = new JTextField();
		tfIDA.setEditable(false);
		tfIDA.setBounds(56, 36, 30, 20);
		panel.add(tfIDA);
		tfIDA.setColumns(10);

		tfIDB = new JTextField();
		tfIDB.setBounds(96, 36, 55, 20);
		panel.add(tfIDB);
		tfIDB.setColumns(10);
		
				// if (cmbTeam.getItemCount() > 0) {
				// cmbTeam.setSelectedIndex(0);
				// }
		
				JButton btnSave = new JButton("Save");
				btnSave.setBounds(726, 7, 68, 23);
				panel.add(btnSave);
				btnSave.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						updateDatabase();
						refreshTournamentTeamsArrayList();

					}
				});
		btnAddPlayer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				// Tools.pToast("This feature is teamporary off");

				String playerId= tfIDB.getText();
				String firstName = tfFirstName.getText();
				String lastName = tfLastName.getText();
				int bat=0;
				int bowl=0;
				int wk=0;
				String teamId = tournamentTeams.get(cmbTeamPos).getTeamId();
				
				if(chbBat.isSelected()){
					bat=1;
				}
				
				if(chbBowl.isSelected()){
					bowl=1;
				}
				
				if(chbWk.isSelected()){
					wk=1;
				}
				
				Player mPlayer = new Player(playerId, firstName, lastName, bat, bowl, wk, teamId);
				addPlayerToPlayersTable(mPlayer);
				setTableByTeam(tournamentTeams.get(cmbTeamPos));
			}
		});
		addTeamsToComboBox();
		cmbTeam.setBounds(378, 8, 114, 20);
		contentPane.add(cmbTeam);

		JButton btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				setTableByTeam(tournamentTeams.get(cmbTeamPos));
			}
		});
		btnRefresh.setBounds(428, 390, 81, 23);
		contentPane.add(btnRefresh);

		JLabel lblCaptain_1 = new JLabel("Players");
		lblCaptain_1.setBounds(58, 434, 56, 14);
		contentPane.add(lblCaptain_1);

		cmbPlayer = new JComboBox();
		cmbPlayer.setBounds(145, 431, 134, 20);
		contentPane.add(cmbPlayer);

		JButton btnCaptain = new JButton("Captain");
		btnCaptain.setBounds(299, 430, 81, 23);
		contentPane.add(btnCaptain);

		JButton btnViceCaptain = new JButton("V. Captain");
		btnViceCaptain.setBounds(390, 430, 102, 23);
		contentPane.add(btnViceCaptain);

		JButton btnAddAll = new JButton("Add All");
		btnAddAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {


				TournamentTeam team = tournamentTeams.get(cmbTeamPos);
				addSquadFromLocalDB(team);
				setTableByTeam(team);

			}
		});
		btnAddAll.setBounds(502, 430, 81, 23);
		contentPane.add(btnAddAll);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				removePlayer(DataUtils.TABLE_PLAYERS, selPlayerIdToRemove);
			
				TournamentTeam team = tournamentTeams.get(cmbTeamPos);
				
				setTableByTeam(team);
			}
		});
		btnDelete.setBounds(329, 390, 89, 23);
		contentPane.add(btnDelete);
		// comboBox.addItem(fontStyle);

		firstView();
	}
	protected void removePlayer(String tableName, String playerId) {

		mOracleHandler.openDatabase();

		try {
			mOracleHandler.runUpdate(SqlHelper.deleteTuple(tableName, "player_id", playerId));
			//Tools.pToast("added");
			System.out.println("Player deleted");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			System.err.println("Player Not deleted");
		}

		mOracleHandler.close();

	}

	protected void addSquadFromLocalDB(TournamentTeam team) {
		// TODO Auto-generated method stub

		ArrayList<Player> al = new ArrayList<>();

		String tableName = "squad_" + team.getTeamCode();
		// Tools.pToast(tableName);

		al = getSquadFromLocalDB(tableName);

		for (int i = 0; i < al.size(); i++) {

			// if(i>0)return;
			// System.out.println(al.get(i).toString());
			Player mPlayer = al.get(i);
			// System.out.println("here "+al.get(i).toString());
			mPlayer.setTeamId(team.getTeamId());
			addPlayerToPlayersTable(mPlayer);
			// addPlayerToSquadsTable(mPlayer.getPlayerId(),team.getTeamId());

		}

	}

	private void addPlayerToPlayersTable(Player mPlayer) {
		// TODO Auto-generated method stub

		mOracleHandler.openDatabase();

		try {
			mOracleHandler.insertIntoPlayersTable(mPlayer);
			System.out.println("Player Added : " + mPlayer.toString());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.err.println("Player adding failed : " + mPlayer.toString());
			e.printStackTrace();
		}

		mOracleHandler.close();

	}

	private void addPlayerToSquadsTable(String playerId, String teamId) {
		// TODO Auto-generated method stub

		mOracleHandler.openDatabase();

		String sql = SqlHelper.insertValueByPs(DataUtils.TABLE_SQUADS, 3);
		String[] arg = { playerId, teamId, "1" };
		try {
			mOracleHandler.runUpdate(sql, arg);
			System.out.println("Player Added To Squad: " + arg[0] + " " + arg[1]);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.err.println("Player adding failed to squad: " + arg[0] + " " + arg[1]);
			// e.printStackTrace();
		}

		mOracleHandler.close();

	}

	private ArrayList<Player> getSquadFromLocalDB(String tableName) {
		// TODO Auto-generated method stub

		System.out.println("getSquadFromLocalDB called");
		ArrayList<Player> al = new ArrayList<>();
		SqliteHandler mSqliteHandler = new SqliteHandler();
		mSqliteHandler.openDatabase(DataUtils.SQLITE_DB_NAME);
		String sql = SqlHelper.getATable(tableName);
		ResultSet rs = null;

		try {
			rs = mSqliteHandler.runQuery(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		al = RSHelper.getSquad(rs);
		mSqliteHandler.close();
		System.out.println("getSquadFromLocalDB finished");
		return al;
	}

	private void firstView() {
		// TODO Auto-generated method stub

		// Tools.pToast("first view");
		System.out.println("First view called");

		if (cmbTeam.getItemCount() > 0) {
			cmbTeam.setSelectedIndex(0);
		}
		setFieldByTeam();

	}

	protected void updateDatabase() {
		// TODO Auto-generated method stub
		String coach, manager, captain, viceCap;
		coach = tfCoach.getText().toString().trim();
		manager = tfManager.getText().toString().trim();
		captain = tfCaptain.getText().toString().trim();
		viceCap = tfViceCap.getText().toString().trim();

		coach = Tools.valString(coach);
		manager = Tools.valString(manager);
		captain = Tools.valString(captain);
		viceCap = Tools.valString(viceCap);

		TournamentTeam team = tournamentTeams.get(cmbTeamPos);

		mOracleHandler.openDatabase();

		String sql = null;

		try {

			sql = SqlHelper.updateTable(DataUtils.TABLE_TOURNAMENT_TEAMS, "coach", coach, "team_id", team.getTeamId());
			mOracleHandler.runSql(sql);

			sql = SqlHelper.updateTable(DataUtils.TABLE_TOURNAMENT_TEAMS, "manager", manager, "team_id",
					team.getTeamId());
			mOracleHandler.runSql(sql);

			sql = SqlHelper.updateTable(DataUtils.TABLE_TOURNAMENT_TEAMS, "captain", captain, "team_id",
					team.getTeamId());
			mOracleHandler.runSql(sql);

			sql = SqlHelper.updateTable(DataUtils.TABLE_TOURNAMENT_TEAMS, "vice_cap", viceCap, "team_id",
					team.getTeamId());
			mOracleHandler.runSql(sql);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		mOracleHandler.close();
	}

	protected void setFieldByTeam() {
		// TODO Auto-generated method stub

		// System.out.println("set field by team called");
		refreshTournamentTeamsArrayList();
		TournamentTeam team = tournamentTeams.get(cmbTeamPos);
		System.out.println(team.toString());

		String coach, manager, captain, viceCap;
		coach = team.getCoach();
		manager = team.getManager();
		captain = team.getCaptain();
		viceCap = team.getViceCaptain();

		coach = Tools.valString(coach);
		manager = Tools.valString(manager);
		captain = Tools.valString(captain);
		viceCap = Tools.valString(viceCap);

		tfCoach.setText(coach);
		tfManager.setText(manager);
		tfCaptain.setText(captain);
		tfViceCap.setText(viceCap);

		setTableByTeam(team);

	}

	private void addTeamsToComboBox() {
		// TODO Auto-generated method stub

		// cmbTeam.removeAllItems();
		// refreshTournamentTeamsArrayList();
		for (int i = 0; i < tournamentTeams.size(); i++) {
			cmbTeam.addItem(tournamentTeams.get(i).getTeamName());
		}

	}

	private void refreshTournamentTeamsArrayList() {
		// TODO Auto-generated method stub

		// ArrayList<String> als = new ArrayList<>();

		mOracleHandler.openDatabase();
		ResultSet rs = null;
		String sql = SqlHelper.getATable(DataUtils.TABLE_TOURNAMENT_TEAMS, "team_id");

		try {
			rs = mOracleHandler.runQuery(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			Tools.pToast("getting tournament teams failed");
		}

		tournamentTeams.clear();
		tournamentTeams = RSHelper.getTournamentTeams(rs);

		mOracleHandler.close();

	}

	private void setRSToTable(ResultSet rs) {
		// TODO Auto-generated method stub
		table.setModel(DbUtils.resultSetToTableModel(rs));
	}

	private void setTableByTeam(TournamentTeam mTournamentTeam) {
		// TODO Auto-generated method stub

		String teamId = mTournamentTeam.getTeamId();
		mOracleHandler.openDatabase();
		if (teamId == null) {
			System.err.println(teamId + " team is null in setTtableByTeam func");
			return;
		}

		ResultSet rs = null;
		try {
			rs = mOracleHandler.runQuery(SqlHelper.getTuples(DataUtils.TABLE_PLAYERS, "team_id", teamId, "player_id"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println(teamId + " runQuery failed");
			// Tools.pToast(teamId + " runQuery failed");
			return;
		}

		if (rs != null) {
			table.setModel(DbUtils.resultSetToTableModel(rs));
		}

		mOracleHandler.close();

	}
}
