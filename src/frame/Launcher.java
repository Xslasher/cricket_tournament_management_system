package frame;
import java.awt.EventQueue;
import java.awt.Menu;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.tools.Tool;

import utils.DataUtils;
import utils.Toast;
import utils.Tools;

import java.awt.Color;
import javax.swing.JCheckBox;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class Launcher {

	private JFrame frame;
	private JTextField tfUserName;
	private JButton btnSignIn;
	private JPasswordField pfPass;
	
//	private JMenuBar menuBar;
//	private JMenuItem menuItem;
//	private JMenu menu;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Launcher window = new Launcher();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Launcher() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle(DataUtils.FRAME_TITLE_LAUNCHER);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		//menuBar = new JMenuBar();
		
		
		JLabel lblNewLabel = new JLabel("User Name");
		lblNewLabel.setBounds(118, 80, 75, 14);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Password");
		lblNewLabel_1.setBounds(118, 117, 75, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		tfUserName = new JTextField();
		tfUserName.setText("admin");
		tfUserName.setBounds(215, 77, 86, 20);
		frame.getContentPane().add(tfUserName);
		tfUserName.setColumns(10);
		
		pfPass = new JPasswordField();
		pfPass.setBounds(215, 114, 86, 20);
		frame.getContentPane().add(pfPass);
		
		btnSignIn = new JButton("Sign In");
		btnSignIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String userName = tfUserName.getText().toString();				
				String userPassword = pfPass.getText().toString();
				
//				Home mHome = new Home();
//				mHome.setVisible(true);
				
				if(userName.equals(DataUtils.APP_LOGIN_USER_NAME)&& userPassword.equals(DataUtils.APP_LOGIN_USER_PASS)){
					//
					//new Toast("Login successfull",5000);
					
					Tools.printMessage("Successfully Loged in");
					Home mHome = new Home();
					mHome.setVisible(true);
				
					
				}else{
					//new Toast("Login Failed",5000);
					Tools.printMessage("Login Failed");
				}
			}
		});
		btnSignIn.setBounds(177, 177, 89, 23);
		frame.getContentPane().add(btnSignIn);
		
		
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnOption = new JMenu("Option");
		menuBar.add(mnOption);
		
		JMenuItem mntmHistory = new JMenuItem("History");
		mnOption.add(mntmHistory);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Database Browser");
		mnOption.add(mntmNewMenuItem);
		
		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		JMenuItem mntmInstruction = new JMenuItem("Instruction");
		mnHelp.add(mntmInstruction);
		
		JMenuItem mntmAbout = new JMenuItem("About");
		mnHelp.add(mntmAbout);
	}
}
