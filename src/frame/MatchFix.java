package frame;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import database.oracle.OracleHandler;
import model.Match;
import model.TournamentTeam;
import net.proteanit.sql.DbUtils;
import oracle.sql.Datum;
import utils.DataUtils;
import utils.PrefUtils;
import utils.RSHelper;
import utils.SqlHelper;
import utils.TimeHelper;
import utils.Tools;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JTextField;
import java.awt.event.ItemListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.awt.event.ItemEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;

public class MatchFix extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField tfDate;
	private JTextField tfMatchType;
	private JTextField tfOver;

	JComboBox cmbTeam1;
	JComboBox cmbTeam2;

	int cmbTeam1Pos = 0;
	int cmbTeam2Pos = 0;

	ArrayList<TournamentTeam> tournamentTeams = new ArrayList<>();
	ArrayList<String> teamsName = new ArrayList<>();

	OracleHandler mOracleHandler = new OracleHandler();
	private JScrollPane scrollPane;
	private JButton btnUndo;
	private JButton btnUnlock;

	private JButton btnFix, btnRefresh, btnLock;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MatchFix frame = new MatchFix();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MatchFix() {
		setTitle(DataUtils.FRAME_TITLE_FIX_MATCH);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 738, 484);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		refreshTournamentTeamsArrayList();

		cmbTeam1 = new JComboBox();

		cmbTeam1.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				JComboBox cmb = (JComboBox) e.getSource();
				// String s = (String) cmb.getSelectedItem();
				cmbTeam1Pos = cmb.getSelectedIndex();

			}
		});
		cmbTeam1.setBounds(141, 8, 174, 20);
		contentPane.add(cmbTeam1);

		cmbTeam2 = new JComboBox();
		cmbTeam2.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				JComboBox cmb = (JComboBox) e.getSource();
				// String s = (String) cmb.getSelectedItem();
				cmbTeam2Pos = cmb.getSelectedIndex();
			}
		});
		cmbTeam2.setBounds(422, 8, 152, 20);
		contentPane.add(cmbTeam2);

		addTeamsToComboBox();
		if (cmbTeam1.getItemCount() > 0) {
			cmbTeam1.setSelectedIndex(0);
			cmbTeam2.setSelectedIndex(0);
		}

		JLabel lblVs = new JLabel("Vs");
		lblVs.setHorizontalAlignment(SwingConstants.CENTER);
		lblVs.setBounds(344, 11, 46, 14);
		contentPane.add(lblVs);

		btnFix = new JButton("Fix");
		btnFix.setVisible(!PrefUtils.getBoolean(PrefUtils.LOCK_FIX, false));
		// if(PrefUtils.getBoolean(PrefUtils.LOCK_FIX, false)){
		//
		// }

		btnFix.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// String matchId,matchNo, team1, team2,date, over, matchType;
				String matchId, team1, team2, date, matchType;
				int matchNo;
				double over;
				int totalDay = 1;

				date = tfDate.getText().toString().trim();
				matchType = tfMatchType.getText().toString().trim().toLowerCase();
				over = Double.parseDouble(tfOver.getText().toString().trim());

				if (matchType.equalsIgnoreCase("test")) {
					Tools.pToast("Test match feature temp off");
					return;
				}

				if (!Tools.isOverValid(matchType, over)) {
					Tools.pToast("Invalid over");
					return;
				}

				if (cmbTeam1Pos == cmbTeam2Pos) {
					Tools.pToast("Invalid match");
					return;
				}

				date = TimeHelper.getDate(date);
				System.out.println(date.toString());

				matchNo = getLastMatchNo() + 1;
				matchId = matchType + (100 + matchNo);
				team1 = tournamentTeams.get(cmbTeam1Pos).getTeamId();
				team2 = tournamentTeams.get(cmbTeam2Pos).getTeamId();

				Match mMatch = new Match(matchId, matchNo, team1, team2, date, over, matchType);
				System.out.println(mMatch.toString());

				fixThisMatch(mMatch);
				setTable();

			}
		});
		btnFix.setBounds(325, 90, 86, 23);
		contentPane.add(btnFix);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 124, 702, 209);
		contentPane.add(scrollPane);

		table = new JTable();
		scrollPane.setViewportView(table);

		JLabel lblDate = new JLabel("Date");
		lblDate.setBounds(141, 53, 37, 14);
		contentPane.add(lblDate);

		tfDate = new JTextField();
		tfDate.setText("20-12-2015");
		tfDate.setBounds(188, 50, 108, 20);
		contentPane.add(tfDate);
		tfDate.setColumns(10);

		JLabel lblType = new JLabel("Type");
		lblType.setBounds(313, 53, 37, 14);
		contentPane.add(lblType);

		tfMatchType = new JTextField();
		tfMatchType.setBounds(363, 50, 61, 20);
		contentPane.add(tfMatchType);
		tfMatchType.setColumns(10);

		JLabel lblOver = new JLabel("Over");
		lblOver.setBounds(481, 53, 46, 14);
		contentPane.add(lblOver);

		tfOver = new JTextField();
		tfOver.setBounds(537, 50, 37, 20);
		contentPane.add(tfOver);
		tfOver.setColumns(10);

		btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setTable();
			}
		});
		btnRefresh.setBounds(389, 344, 89, 23);
		contentPane.add(btnRefresh);

		btnUndo = new JButton("Undo");
		btnUndo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String matchType = PrefUtils.getString(PrefUtils.TOURNAMENT_MATCH_TYPE, "");
				String tableName = Tools.getTableNameFromMatchType(matchType);
				tableName = DataUtils.TABLE_MATCH;
				int matchNo;

				matchNo = getLastMatchNo();

				mOracleHandler.openDatabase();
				try {
					mOracleHandler.runSql(SqlHelper.deleteTuple(tableName, "match_no", matchNo));
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				mOracleHandler.close();

				setTable();
			}
		});
		btnUndo.setBounds(278, 344, 89, 23);
		contentPane.add(btnUndo);

		btnLock = new JButton("Lock");
		btnLock.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				btnFix.setEnabled(false);
				;
				btnUndo.setEnabled(false);

				PrefUtils.putBoolean(PrefUtils.LOCK_FIX, true);
				PrefUtils.putBoolean(PrefUtils.LOCK_UNDO, true);
			}
		});
		btnLock.setBounds(611, 36, 89, 49);
		contentPane.add(btnLock);

		btnUnlock = new JButton("Unlock");
		btnUnlock.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				btnFix.setEnabled(true);
				btnUndo.setEnabled(true);

				PrefUtils.putBoolean(PrefUtils.LOCK_FIX, false);
				PrefUtils.putBoolean(PrefUtils.LOCK_UNDO, false);

			}
		});
		btnUnlock.setBounds(611, 412, 89, 23);
		contentPane.add(btnUnlock);

		firstView();
	}

	protected void fixThisMatch(Match mMatch) {
		// TODO Auto-generated method stub

		String matchType = PrefUtils.getString(PrefUtils.TOURNAMENT_MATCH_TYPE, "");
		String tableName = Tools.getTableNameFromMatchType(matchType);
		tableName = DataUtils.TABLE_MATCH;

		mOracleHandler.openDatabase();

		String matchId, matchNo, team1, team2, date, over;

		matchNo = String.valueOf(mMatch.getMatchNo());
		matchId = mMatch.getMatchId();
		team1 = mMatch.getTeam1();
		team2 = mMatch.getTeam2();
		date = mMatch.getDate();
		over = String.valueOf(mMatch.getOver());

		String[] arg = { matchNo, matchId, team1, team2, date, matchType, over };
		String sql = SqlHelper.insertValueByPs(tableName, 7);

		try {
			mOracleHandler.runUpdate(sql, arg);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		sql = SqlHelper.insertValueByPs(DataUtils.TABLE_TOSS, 3);
		String[] arg2 = { matchId, "", "" };
		try {
			mOracleHandler.runUpdate(sql, arg2);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

		sql = SqlHelper.insertValueByPs(DataUtils.TABLE_INNINGS, 5);
		String[] arg3 = { matchId, "", "", "", "" };
		try {
			mOracleHandler.runUpdate(sql, arg3);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

		sql = SqlHelper.insertValueByPs(DataUtils.TABLE_RESULT, 3);
		String[] arg4 = { matchId, "", "" };
		try {
			mOracleHandler.runUpdate(sql, arg4);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

		mOracleHandler.close();

		createFsbb(matchId);

	}

	private void createFsbb(String matchId) {
		// TODO Auto-generated method stub

		mOracleHandler.openDatabase();

		String fSquadTableName = DataUtils.TABLE_FINAL_SQUAD_PREFIX + matchId;
		System.out.println(fSquadTableName);
		try {

			if (mOracleHandler.isTableExist(fSquadTableName)) {
				mOracleHandler.runUpdate(SqlHelper.dropTable(fSquadTableName));
			}

			mOracleHandler.runUpdate(SqlHelper.createTable(fSquadTableName));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			System.err.println("Final Squad Table Not created");
		}
		
		mOracleHandler.close();
		
		TournamentTeam a,b;
		a=tournamentTeams.get(cmbTeam1Pos);
		b=tournamentTeams.get(cmbTeam2Pos);
		
		String tableInn1Bat = matchId + DataUtils.TABLE_BATTING_INFIX + a.getTeamId();
		String tableInn1Bowl = matchId + DataUtils.TABLE_BOWLING_INFIX + b.getTeamId();
		
		String tableInn2Bat = matchId + DataUtils.TABLE_BATTING_INFIX + b.getTeamId();
		String tableInn2Bowl = matchId + DataUtils.TABLE_BOWLING_INFIX + a.getTeamId();
		
		String tableName = null;
		
		dropTableIfExits(tableInn1Bat);
		dropTableIfExits(tableInn1Bowl);
		
		dropTableIfExits(tableInn2Bat);
		dropTableIfExits(tableInn2Bowl);
		
		mOracleHandler.openDatabase();
		try {

			mOracleHandler.runUpdate(SqlHelper.createTable(tableInn1Bat));
			mOracleHandler.runUpdate(SqlHelper.createTable(tableInn1Bowl));
			mOracleHandler.runUpdate(SqlHelper.createTable(tableInn2Bat));
			mOracleHandler.runUpdate(SqlHelper.createTable(tableInn2Bowl));
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			System.err.println("FSBB  Tables Not created");
		}

		mOracleHandler.close();
	}

	private void dropTableIfExits(String tableName) {
		// TODO Auto-generated method stub
		mOracleHandler.openDatabase();
		if (mOracleHandler.isTableExist(tableName)) {
			try {
				mOracleHandler.runUpdate(SqlHelper.dropTable(tableName));
				System.out.println(tableName + " table Dropped");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.err.println(tableName + " table not Dropped");
			}
		}
		mOracleHandler.close();
	}

	protected int getLastMatchNo() {
		// TODO Auto-generated method stub

		String matchType = PrefUtils.getString(PrefUtils.TOURNAMENT_MATCH_TYPE, "");
		String tableName = Tools.getTableNameFromMatchType(matchType);
		tableName = DataUtils.TABLE_MATCH;

		mOracleHandler.openDatabase();
		ResultSet rs = null;
		String sql = SqlHelper.getATable(tableName);

		try {
			rs = mOracleHandler.runQuery(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			// Tools.pToast("getting tournament teams failed");
		}

		int n = RSHelper.getTotalResult(rs);

		mOracleHandler.close();

		return n;
	}

	private void firstView() {
		// TODO Auto-generated method stub
		tfMatchType.setEditable(false);
		tfOver.setEditable(false);

		String type = null;

		String matchType = PrefUtils.getString(PrefUtils.TOURNAMENT_MATCH_TYPE, "");
		int over = getOverFromMatchType(matchType);

		tfMatchType.setText(matchType.toUpperCase());
		tfOver.setText(String.valueOf(over));

		setTable();

	}

	private int getOverFromMatchType(String matchType) {
		// TODO Auto-generated method stub

		int over = 0;

		if (matchType.equalsIgnoreCase(DataUtils.MATCH_ODI)) {
			over = DataUtils.MAX_OVER_ODI;
		} else if (matchType.equalsIgnoreCase(DataUtils.MATCH_T20)) {
			over = DataUtils.MAX_OVER_T20;
		} else if (matchType.equalsIgnoreCase(DataUtils.MATCH_TEST)) {
			over = 90;
		}

		return over;
	}

	// protected String getTableNameFromMatchType(String matchType) {
	// // TODO Auto-generated method stub
	// String tableName = null;
	//
	// if (matchType.equalsIgnoreCase(DataUtils.MATCH_ODI)) {
	// tableName = DataUtils.TABLE_MATCH_ODI;
	// } else if (matchType.equalsIgnoreCase(DataUtils.MATCH_T20)) {
	// tableName = DataUtils.TABLE_MATCH_T20;
	// } else if (matchType.equalsIgnoreCase(DataUtils.MATCH_TEST)) {
	// tableName = DataUtils.TABLE_MATCH_TEST;
	// }
	//
	// tableName = DataUtils.TABLE_MATCH;
	//
	// return tableName;
	// }

	private void addTeamsToComboBox() {
		// TODO Auto-generated method stub

		// cmbTeam.removeAllItems();
		// refreshTournamentTeamsArrayList();
		for (int i = 0; i < tournamentTeams.size(); i++) {
			cmbTeam1.addItem(tournamentTeams.get(i).getTeamName());
			cmbTeam2.addItem(tournamentTeams.get(i).getTeamName());
		}

	}

	private void refreshTournamentTeamsArrayList() {
		// TODO Auto-generated method stub

		// ArrayList<String> als = new ArrayList<>();

		mOracleHandler.openDatabase();
		ResultSet rs = null;
		String sql = SqlHelper.getATable(DataUtils.TABLE_TOURNAMENT_TEAMS, "team_id");

		try {
			rs = mOracleHandler.runQuery(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			Tools.pToast("getting tournament teams failed");
		}

		tournamentTeams.clear();
		tournamentTeams = RSHelper.getTournamentTeams(rs);

		mOracleHandler.close();

	}

	private void setRSToTable(ResultSet rs) {
		// TODO Auto-generated method stub
		table.setModel(DbUtils.resultSetToTableModel(rs));
	}

	private void setTable() {
		// TODO Auto-generated method stub

		String matchType = PrefUtils.getString(PrefUtils.TOURNAMENT_MATCH_TYPE, "");
		String tableName = Tools.getTableNameFromMatchType(matchType);
		tableName = DataUtils.TABLE_MATCH;

		mOracleHandler.openDatabase();
		ResultSet rs = null;
		String sql = SqlHelper.getATable(tableName, "match_id");
		// String sql = SqlHelper.getTablesWithJoin(tableName,
		// DataUtils.TABLE_ALL_CRIC_TEAMS, team, col2)

		try {
			rs = mOracleHandler.runQuery(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			Tools.pToast("set table failed");
		}

		table.setModel(DbUtils.resultSetToTableModel(rs));

		mOracleHandler.close();

	}
}
