package frame;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import database.oracle.OracleHandler;
import database.sqlite.SqliteHandler;
import model.Team;
import net.proteanit.sql.DbUtils;
import utils.DataUtils;
import utils.RSHelper;
import utils.SqlHelper;
import utils.Toast;
import utils.Tools;

public class SeeAllTeams extends JFrame {

	private JPanel contentPane;
	private JTable table;
	
	private JTextField tfTeamId;
	private JTextField tfTeamName;
	private JTextField tfTeamCode;
	
	
	OracleHandler mOracleHandler;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SeeAllTeams frame = new SeeAllTeams();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SeeAllTeams() {
		setTitle(DataUtils.FRAME_TITLE_ALL_TEAMS);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 641, 461);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		mOracleHandler = new OracleHandler();
//		mOracleHandler.openDatabase();
		
		//setTable(DataUtils.TABLE_ALL_CRIC_TEAMS);
		

		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 96, 605, 282);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JButton btnAddTeam = new JButton("Add");
		btnAddTeam.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//pToast("not working yet");
				
				
				String sql=null;
				
				String teamId,teamName,teamCode;
				teamId = tfTeamId.getText().toString().trim();
				teamName = tfTeamName.getText().toString().trim();
				teamCode = tfTeamCode.getText().toString().trim();
				
				//pToast(teamId+teamName+teamCode);
				
				if(teamId==null || teamName==null || teamCode==null){
					pToast("Enter valid data");
					return;
				}else if(teamId.length()<1 || teamName.length()<1 || teamCode.length()<1){
					pToast("Enter valid data");
					return;
				}
				
				String[] arr = {teamId,teamName,teamCode};
				//String[] arr = {"sflds","dsfsds","dsdfsd"};
				
				//sql = "insert into all_cric_teams values ('ddd','edd','fdd')";
				//sql = "insert into all_cric_teams (team_id,team_name, team_code) values ('gh','gh','gh')";
				//Tools.pToast(sql,1);
				
				mOracleHandler.openDatabase();
				//mOracleHandler.insertData("all_cric_teams", "t", "t", "t");
				
				try {
					//mOracleHandler.insertIntoAllCricTeams("",arr);
					mOracleHandler.runUpdate(SqlHelper.insertValueByPs("all_cric_teams", arr.length), arr);
					//mOracleHandler.runSql(SqlHelper.insertIntoAllCricTeams(arr));
					//mOracleHandler.runSql(sql);
					//mOracleHandler.insertValue(sql);
					//mOracleHandler.runUpdatePs(SqlHelper.insetValueByPs("all_cric_teams", arr.length), arr);
					Tools.pToast("data inserted");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					Tools.pToast("data not inserted");
				}
				
				
				mOracleHandler.close();	
				setTable("all_cric_teams");
				
			}
		});
		btnAddTeam.setBounds(489, 62, 89, 23);
		contentPane.add(btnAddTeam);
		
		JLabel lblAllTeams = new JLabel("All Teams");
		lblAllTeams.setHorizontalAlignment(SwingConstants.CENTER);
		lblAllTeams.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblAllTeams.setBounds(273, 11, 89, 23);
		contentPane.add(lblAllTeams);
		
		JButton btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setTable("all_cric_teams");
			}
		});
		btnRefresh.setBounds(331, 389, 89, 23);
		contentPane.add(btnRefresh);
		
		JButton btnUpdate = new JButton("Update From Remote");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				ArrayList<Team> al = new ArrayList<>();
				al = getAllTeam();
				
				//mOracleHandler.openDatabase();
				
				updateTeamList(al);
				setTable("all_cric_teams");
				
				///mOracleHandler.close();
				
			}
		});
		btnUpdate.setToolTipText("Update From Remote");
		btnUpdate.setBounds(220, 389, 89, 23);
		contentPane.add(btnUpdate);
		
		JLabel lblTeamName = new JLabel("Team Name");
		lblTeamName.setBounds(157, 66, 71, 14);
		contentPane.add(lblTeamName);
		
		tfTeamId = new JTextField();
		tfTeamId.setBounds(61, 63, 86, 20);
		contentPane.add(tfTeamId);
		tfTeamId.setColumns(10);
		
		JLabel lblTeamId = new JLabel("Team ID");
		lblTeamId.setBounds(10, 66, 52, 14);
		contentPane.add(lblTeamId);
		
		tfTeamName = new JTextField();
		tfTeamName.setBounds(240, 63, 104, 20);
		contentPane.add(tfTeamName);
		tfTeamName.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Code");
		lblNewLabel.setBounds(371, 66, 33, 14);
		contentPane.add(lblNewLabel);
		
		tfTeamCode = new JTextField();
		tfTeamCode.setBounds(409, 63, 52, 20);
		contentPane.add(tfTeamCode);
		tfTeamCode.setColumns(10);
		
		firstView();
	}
	
	
	private void firstView() {
		// TODO Auto-generated method stub
		setTable(DataUtils.TABLE_ALL_CRIC_TEAMS);
	}

	protected void updateTeamList(ArrayList<Team> al) {
		// TODO Auto-generated method stub
		mOracleHandler.openDatabase();
		try {
			mOracleHandler.runUpdate(SqlHelper.deleteAllTuples("all_cric_teams"));
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
		
		String sql = SqlHelper.insertValueByPs("all_cric_teams", 3);
		
		for (int i = 0; i < al.size(); i++) {
			Team team = new Team();
			team = al.get(i);
			//System.out.println(team.toString());
			
			String[] arr = {team.getTeamId(),team.getTeamName(),team.getTeamCode()};
//			String s = SqlHelper.insertIntoAllCricTeams(arr);
//			System.out.println(s+";");
			try {
				mOracleHandler.runUpdate(sql, arr);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Tools.pToast("Update team list failed");
				break;
			}
		}
		
		mOracleHandler.close();
	}

	protected ArrayList<Team> getAllTeam() {
		// TODO Auto-generated method stub
		
		
		ResultSet rs = null;
		ArrayList<Team> al = new ArrayList<>();
		
		SqliteHandler mSqliteHandler = new SqliteHandler();
		try {
			mSqliteHandler.openDatabase(DataUtils.SQLITE_DB_NAME);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		try {
			rs = mSqliteHandler.runQuery(SqlHelper.getATable("all_cric_teams"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		al = RSHelper.getAllTeamBase(rs);
		mSqliteHandler.close();
		
//		for (int i = 0; i < al.size(); i++) {
//			TeamBase team = new TeamBase();
//			team = al.get(i);
//
//			System.out.println(team.toString());
//		}
		
		return al;
	}
	
	
	
	private void setTable(String tableName) {
		// TODO Auto-generated method stub
		ResultSet rs = null;
		if(tableName==null){
			System.err.println(tableName+" tableName is null in setTtable func");			
			return;
		}
		String oc="team_id";
		mOracleHandler.openDatabase();
		try {
			rs = mOracleHandler.runQuery(SqlHelper.getATable(tableName,oc));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println(tableName+" runQuery failed");
			Tools.pToast(tableName+" runQuery failed");
			return;
		}
		
		table.setModel(DbUtils.resultSetToTableModel(rs));
		mOracleHandler.close();
		
	}
	
	private void setRSToTable(ResultSet rs) {
		// TODO Auto-generated method stub
		table.setModel(DbUtils.resultSetToTableModel(rs));
	}

	public void pToast(String s){
		Toast mToast = new Toast(s, 1000);
		mToast.setVisible(true);
	}
}
