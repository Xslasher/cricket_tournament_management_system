package frame;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import utils.DataUtils;
import utils.PrefUtils;
import utils.RSHelper;
import utils.SqlHelper;
import utils.Tools;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.management.modelmbean.ModelMBeanOperationInfo;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JSpinner;
import javax.swing.border.LineBorder;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;

import database.oracle.OracleHandler;
import net.proteanit.sql.DbUtils;

import java.awt.Color;
import java.awt.event.ItemListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ItemEvent;
import javax.swing.ButtonGroup;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MatchPlay extends JFrame {

	private JPanel contentPane;
	JLabel lblMatchInfo;
	JComboBox cmbIng;
	JLabel lblMatchStatus;

//	 String team1Id = PrefUtils.getString(PrefUtils.TEAM1_ID, "");
//	 String team2Id = PrefUtils.getString(PrefUtils.TEAM2_ID, "");
	// String team1Name = PrefUtils.getString(PrefUtils.TEAM1_NAME, "");
	// String team2Name = PrefUtils.getString(PrefUtils.TEAM2_NAME, "");
	// String team1Code = PrefUtils.getString(PrefUtils.TEAM1_CODE, "");
	// String team2Code = PrefUtils.getString(PrefUtils.TEAM2_CODE, "");
	String matchId = PrefUtils.getString(PrefUtils.CURRENT_MATCH_ID, "");

	// String inn1BatTeamId = PrefUtils.getString(PrefUtils.INN1_BAT_TEAM_ID,
	// "");
	// String inn1BowlTeamId = PrefUtils.getString(PrefUtils.INN1_BOWL_TEAM_ID,
	// "");
	// String inn2BatTeamId = PrefUtils.getString(PrefUtils.INN2_BAT_TEAM_ID,
	// "");
	// String inn2BowlTeamId = PrefUtils.getString(PrefUtils.INN2_BOWL_TEAM_ID,
	// "");

	String inn1BatTeamId;
	String inn1BowlTeamId;
	String inn2BatTeamId;
	String inn2BowlTeamId;

	String cbatTable, cbowlTable;

	// String curBatCode,curBowlCode;

	String curBattingTeamId, curBowlingTeamId;

	private JTable tableBatting;
	private JTable tableBowling;
	private JTextField tfBallNo;
	private JTable tableSquad;

	JRadioButton rbOk, rbNb, rbWd, rbDead;
	JRadioButton rbBat, rbLb;
	JSpinner spBatLbRun, spByeRun;
	JCheckBox chbBye;
	JRadioButton rbBowled, rbCatch, rbRunOut, rbStamping;
	JButton btnSave, btnStrikeRotet, btnNewBall;

	OracleHandler mOracleHandler = new OracleHandler();
	private final ButtonGroup bgBallStatus = new ButtonGroup();
	private final ButtonGroup bgRunType = new ButtonGroup();
	private final ButtonGroup bgOutType = new ButtonGroup();

	JComboBox cmbBatBowl;
	private JButton btnReset;

	String selPlayerId;
	private JButton btnResetAll;

	JLabel lblBowling, lblScore;
	JLabel lblRunRate;
	private final ButtonGroup bgBatsmanBowler = new ButtonGroup();
	private JButton btnOver;
	
	JLabel lblStrikeStatus;
	
	JComboBox cmbTeam;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MatchPlay frame = new MatchPlay();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MatchPlay() {

		setTitle(DataUtils.FRAME_TITLE_PLAY_MATCH);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 931, 590);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnOption = new JMenu("Option");
		menuBar.add(mnOption);

		JMenuItem mntmFinalSquad = new JMenuItem("Final Squad");
		mntmFinalSquad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				FinalSquad frame = new FinalSquad();
				frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame.setVisible(true);

			}
		});
		mnOption.add(mntmFinalSquad);

		JMenuItem mntmToss = new JMenuItem("Toss");
		mntmToss.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				TossPlay frame = new TossPlay();
				frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame.setVisible(true);

			}
		});
		mnOption.add(mntmToss);

		JMenuItem mntmMatchDetails = new JMenuItem("Match Details");
		mnOption.add(mntmMatchDetails);
		
		JMenuItem mntmPointResult = new JMenuItem("Point & Result");
		mntmPointResult.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				PointTable frame = new PointTable();
				frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame.setVisible(true);
			}
		});
		mnOption.add(mntmPointResult);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		lblMatchStatus = new JLabel("Match Status");
		lblMatchStatus.setBounds(10, 506, 214, 14);
		contentPane.add(lblMatchStatus);

		cmbIng = new JComboBox();

		cmbIng.addItem("1st Innings");
		cmbIng.addItem("2st Innings");
		cmbIng.setSelectedIndex(0);
		// cmbBatBowl.setSelectedIndex(0);
		cmbIng.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				// JComboBox cmb = (JComboBox) e.getSource();
				// // String s = (String) cmb.getSelectedItem();
				// int n = cmb.getSelectedIndex();

				refreshTask();
				refreshTables();
				//refreshStrikeStatus();
				refreshScoreField();
			}
		});

		cmbIng.setBounds(268, 503, 172, 20);
		contentPane.add(cmbIng);

		lblMatchInfo = new JLabel("match info");
		lblMatchInfo.setHorizontalAlignment(SwingConstants.TRAILING);
		lblMatchInfo.setBounds(576, 506, 329, 14);
		contentPane.add(lblMatchInfo);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 35, 895, 84);
		contentPane.add(scrollPane);

		tableBatting = new JTable();
		scrollPane.setViewportView(tableBatting);

		lblScore = new JLabel("Ban 24/2 (5.2)");
		lblScore.setBounds(10, 11, 98, 14);
		contentPane.add(lblScore);

		lblBowling = new JLabel("RSA Bowling");
		lblBowling.setBounds(10, 130, 93, 14);
		contentPane.add(lblBowling);

		// lblBowling.setText(c);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 155, 895, 109);
		contentPane.add(scrollPane_1);

		tableBowling = new JTable();
		scrollPane_1.setViewportView(tableBowling);

		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBounds(10, 275, 498, 145);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblBallStatus = new JLabel("Ball status");
		lblBallStatus.setBounds(10, 36, 77, 14);
		panel.add(lblBallStatus);

		JLabel lblRunType = new JLabel("Run Type");
		lblRunType.setBounds(10, 61, 77, 14);
		panel.add(lblRunType);

		JLabel lblOutType = new JLabel("Out Type");
		lblOutType.setBounds(10, 86, 77, 14);
		panel.add(lblOutType);

		rbOk = new JRadioButton("Ok");
		rbOk.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				enableAllRun(true);
				enableAllOut(true);
			}
		});
		bgBallStatus.add(rbOk);
		rbOk.setBounds(93, 32, 65, 23);
		panel.add(rbOk);

		rbNb = new JRadioButton("Nb");
		rbNb.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				enableAllRun(true);
				enableAllOut(false);
				rbRunOut.setEnabled(true);
			}
		});
		bgBallStatus.add(rbNb);
		rbNb.setBounds(160, 32, 65, 23);
		panel.add(rbNb);

		rbWd = new JRadioButton("Wd");
		rbWd.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				enableAllRun(false);
				enableAllOut(false);
				rbRunOut.setEnabled(true);
				rbStamping.setEnabled(true);

			}
		});
		bgBallStatus.add(rbWd);
		rbWd.setBounds(227, 32, 65, 23);
		panel.add(rbWd);

		rbDead = new JRadioButton("Dead");
		rbDead.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				enableAllRun(false);
				enableAllOut(false);
			}
		});
		bgBallStatus.add(rbDead);
		rbDead.setBounds(294, 32, 77, 23);
		panel.add(rbDead);

		JLabel lblBallNo = new JLabel("Ball No");
		lblBallNo.setBounds(10, 11, 77, 14);
		panel.add(lblBallNo);

		tfBallNo = new JTextField();
		tfBallNo.setEditable(false);
		tfBallNo.setBounds(93, 8, 53, 20);
		panel.add(tfBallNo);
		tfBallNo.setColumns(10);

		btnNewBall = new JButton("New Ball");
		btnNewBall.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewBall.setBounds(156, 7, 107, 23);
		panel.add(btnNewBall);

		spBatLbRun = new JSpinner();
		spBatLbRun.setBounds(227, 58, 43, 20);
		panel.add(spBatLbRun);

		chbBye = new JCheckBox("Bye");
		chbBye.setEnabled(false);
		chbBye.setBounds(296, 57, 53, 23);
		panel.add(chbBye);

		spByeRun = new JSpinner();
		spByeRun.setEnabled(false);
		spByeRun.setBounds(355, 58, 39, 20);
		panel.add(spByeRun);

		rbBowled = new JRadioButton("Bowled");
		// rbBowled.setActionCommand("Bowled");
		bgOutType.add(rbBowled);
		rbBowled.setBounds(93, 82, 65, 23);
		panel.add(rbBowled);

		rbCatch = new JRadioButton("Catch");
		// rbCatch.setActionCommand("rbCatch");
		bgOutType.add(rbCatch);
		rbCatch.setBounds(160, 82, 65, 23);
		panel.add(rbCatch);

		rbRunOut = new JRadioButton("Run Out");
		// rbRunOut.setActionCommand("rbOk");
		bgOutType.add(rbRunOut);
		rbRunOut.setBounds(227, 82, 65, 23);
		panel.add(rbRunOut);

		rbStamping = new JRadioButton("Stamping");
		bgOutType.add(rbStamping);
		rbStamping.setBounds(294, 82, 100, 23);
		panel.add(rbStamping);

		lblStrikeStatus = new JLabel("Strike Status");
		lblStrikeStatus.setHorizontalAlignment(SwingConstants.TRAILING);
		lblStrikeStatus.setBounds(258, 120, 230, 14);
		panel.add(lblStrikeStatus);

		btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (rbDead.isSelected()) {
					Tools.pToast("Dead Ball");
					return;
				}

				String batsmanId, bowlerId;

				// batsmanId = getAStringValueFromDB(cbatTable, "player_id",
				// "on_str", 1);
				// bowlerId = getAStringValueFromDB(cbowlTable, "player_id",
				// "on_str", 1);

				batsmanId = getStrikePlayer(cbatTable);
				bowlerId = getStrikePlayer(cbowlTable);

				String strStatus = "Bat: " + batsmanId + "  Bowl: " + bowlerId;
				lblStrikeStatus.setText(strStatus);
				
				if(batsmanId==null || bowlerId ==null){
					Tools.pToast("No Batsmant or Baller on Strike");
					return;
				}
				

				updateBattingTable(batsmanId);
				updateBowlingTable(bowlerId);

				refreshTables();
				refreshScoreField();
				

			}
		});
		btnSave.setBounds(10, 116, 89, 23);
		panel.add(btnSave);

		btnStrikeRotet = new JButton("Strike Rotet");
		btnStrikeRotet.setBounds(273, 7, 121, 23);
		panel.add(btnStrikeRotet);

		rbBat = new JRadioButton("Bat");
		bgRunType.add(rbBat);
		rbBat.setBounds(93, 57, 65, 23);
		panel.add(rbBat);

		rbLb = new JRadioButton("Lb");
		rbLb.setEnabled(false);
		// rbLb.setActionCommand("");
		bgRunType.add(rbLb);
		rbLb.setBounds(160, 57, 65, 23);
		panel.add(rbLb);

		btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// setAllRun(false);
				// setAllOut(false);

				bgBallStatus.clearSelection();
				bgOutType.clearSelection();
				bgRunType.clearSelection();

				enableAllRun(true);
				enableAllOut(true);

			}
		});
		btnReset.setBounds(109, 116, 89, 23);
		panel.add(btnReset);
		
		btnOver = new JButton("Over");
		btnOver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String batsmanId, bowlerId;

				batsmanId = getStrikePlayer(cbatTable);
				bowlerId = getStrikePlayer(cbowlTable);

//				String strStatus = "Bat: " + batsmanId + "  Bowl: " + bowlerId;
//				lblStrikeStatus.setText(strStatus);
				
				
				incAFieldByPlayerId(cbowlTable, "over", bowlerId);
				updateAFieldByPlayerId(cbowlTable,"cov_ball",""+0,bowlerId);
				
				updateAFieldByPlayerId(cbowlTable,"on_str",""+0,bowlerId);
				
				refreshTables();
				
				refreshStrikeStatus();
				
				
			}
		});
		btnOver.setBounds(399, 7, 89, 23);
		panel.add(btnOver);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_1.setBounds(535, 275, 370, 145);
		contentPane.add(panel_1);
		panel_1.setLayout(null);

		getMatchStatusFromDB();
		cmbBatBowl = new JComboBox();

		cmbBatBowl.setBounds(10, 11, 111, 20);
		panel_1.add(cmbBatBowl);
		cmbBatBowl.addItem("Batting");
		cmbBatBowl.addItem("Bowling");
		cmbBatBowl.setSelectedIndex(0);

		cmbBatBowl.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setTableSquad();
			}
		});

		JScrollPane scrollPane_2 = new JScrollPane();

		scrollPane_2.setBounds(10, 46, 350, 88);
		panel_1.add(scrollPane_2);

		tableSquad = new JTable();
		tableSquad.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				int row = tableSquad.getSelectedRow();
				selPlayerId = tableSquad.getModel().getValueAt(row, 0).toString();
				Tools.pToast(selPlayerId);

			}
		});
		scrollPane_2.setViewportView(tableSquad);

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// String tableName = DataUtils.TABLE_FINAL_SQUAD_PREFIX+
				// currentMatchId;
				// addToFinalSquad(tableName,selPlayerIdToAdd,selTeamId);
				// setTableFinalSquadByTeamId(selTeamId);

				if (cmbBatBowl.getSelectedIndex() == 0) {
					addToBatting(selPlayerId);
				} else {
					addToBowling(selPlayerId);
				}

				refreshTables();

			}
		});
		btnAdd.setBounds(131, 10, 70, 23);
		panel_1.add(btnAdd);

		JButton btnUndo = new JButton("Undo");
		btnUndo.setEnabled(false);
		btnUndo.setBounds(296, 10, 64, 23);
		panel_1.add(btnUndo);

		JButton btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				refreshTask();
				refreshTables();
				refreshScoreField();
				refreshStrikeStatus();
				

			}
		});
		btnRefresh.setBounds(450, 502, 89, 23);
		contentPane.add(btnRefresh);

		btnResetAll = new JButton("Reset All");
		btnResetAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				wipeTable(cbatTable);
				wipeTable(cbowlTable);

				refreshTables();

			}
		});
		btnResetAll.setBounds(807, 7, 98, 23);
		contentPane.add(btnResetAll);

		lblRunRate = new JLabel("RR 5.3");
		lblRunRate.setBounds(173, 10, 69, 14);
		contentPane.add(lblRunRate);

		final JSpinner spBowlerPos = new JSpinner();
		spBowlerPos.setBounds(661, 127, 46, 20);
		contentPane.add(spBowlerPos);

		final JRadioButton rbBatsman = new JRadioButton("Batsman");
		bgBatsmanBowler.add(rbBatsman);
		rbBatsman.setBounds(508, 126, 70, 23);
		contentPane.add(rbBatsman);

		final JRadioButton rbBowler = new JRadioButton("Bowler");
		bgBatsmanBowler.add(rbBowler);
		rbBowler.setBounds(580, 126, 62, 23);
		contentPane.add(rbBowler);

		JButton btnOffStr = new JButton("Off STR");
		btnOffStr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				int pos = (int) spBowlerPos.getValue();
				// Tools.pToast(""+pos);

				if (pos <= 0) {
					return;
				}

				if (rbBatsman.isSelected()) {
					setStrike(cbatTable, pos, 0);
				} else if (rbBowler.isSelected()) {
					setStrike(cbowlTable, pos, 0);
				} else {
					return;
				}

				refreshTables();
			}
		});
		btnOffStr.setBounds(816, 126, 89, 23);
		contentPane.add(btnOffStr);

		JButton btnOnStrike = new JButton("On STR");
		btnOnStrike.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				int pos = (int) spBowlerPos.getValue();
				// Tools.pToast(""+pos);
				if (pos <= 0) {
					return;
				}

				if (rbBatsman.isSelected()) {
					setStrike(cbatTable, pos, 1);
				} else if (rbBowler.isSelected()) {
					setStrike(cbowlTable, pos, 1);
				} else {
					return;
				}

				// giveStrike()
				refreshTables();

			}
		});
		btnOnStrike.setBounds(728, 126, 78, 23);
		contentPane.add(btnOnStrike);
		
		cmbTeam = new JComboBox();
		cmbTeam.setBounds(10, 431, 115, 20);
		contentPane.add(cmbTeam);
		cmbTeam.addItem(PrefUtils.getString(PrefUtils.TEAM1_ID, ""));
		cmbTeam.addItem(PrefUtils.getString(PrefUtils.TEAM2_ID, ""));
		cmbTeam.setSelectedIndex(0);
		
		
		JButton btnWin = new JButton("Win");
		
		btnWin.setBounds(135, 430, 89, 23);
		contentPane.add(btnWin);
	
		btnWin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String winTeamId = (String) cmbTeam.getSelectedItem();
				//Tools.pToast(winTeamId);
				String looseTeamId;
				
				if(winTeamId.equals(PrefUtils.getString(PrefUtils.TEAM1_ID, ""))){
					looseTeamId = PrefUtils.getString(PrefUtils.TEAM2_ID, "");
				}else{
					looseTeamId = PrefUtils.getString(PrefUtils.TEAM1_ID, "");
				}
				
				System.out.println("Win: " + winTeamId + "  Loose: " + looseTeamId);
				
				
				String tableName;
				updateAField(DataUtils.TABLE_RESULT, "win_team", winTeamId, "match_id", matchId);
				updateAField(DataUtils.TABLE_RESULT, "lose_team", looseTeamId, "match_id", matchId);
				
				incAField(DataUtils.TABLE_POINT, "play", "team_id", winTeamId);
				incAField(DataUtils.TABLE_POINT, "play", "team_id", looseTeamId);
				
				incAField(DataUtils.TABLE_POINT, "won", "team_id", winTeamId);
				incAField(DataUtils.TABLE_POINT, "lost", "team_id", looseTeamId);
				
				
				
				
				
				
			}
		});
		
		firstView();
	}

	private void firstView() {
		// TODO Auto-generated method stub

		String matchInfo = Tools.getMatchDetailsFromPref();
		lblMatchInfo.setText(matchInfo);

		refreshTables();

		refreshScoreField();
	}

	protected void updateAField(String tableName, String setCol, String setValue, String conCol, String conValue) {
		// TODO Auto-generated method stub
		
		mOracleHandler.openDatabase();

		String sql = SqlHelper.updateTable(tableName, setCol, setValue, conCol, conValue);
		try {
			mOracleHandler.runUpdate(sql);
			//System.out.println("Strike Rotated");
			// Tools.pToast("Strike Rotated");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}

		mOracleHandler.close();
	}
	
	protected void updateAField(String tableName, String setCol, String setValue, String conCol, int conValue) {
		// TODO Auto-generated method stub
		
		mOracleHandler.openDatabase();
		
		String sql = SqlHelper.updateTable(tableName, setCol, setValue, conCol, conValue);
		try {
			mOracleHandler.runUpdate(sql);
			//System.out.println("Strike Rotated");
			// Tools.pToast("Strike Rotated");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		
		mOracleHandler.close();
	}
	

	protected void refreshStrikeStatus() {
		// TODO Auto-generated method stub
		String batsmanId, bowlerId;

		batsmanId = getStrikePlayer(cbatTable);
		bowlerId = getStrikePlayer(cbowlTable);

		String strStatus = "Bat: " + batsmanId + "  Bowl: " + bowlerId;
		lblStrikeStatus.setText(strStatus);
	}

	protected void updateAFieldByPlayerId(String tableName, String col, String value, String playerId) {
		// TODO Auto-generated method stub
		mOracleHandler.openDatabase();

		String sql = SqlHelper.updateTable(tableName, col, value, "player_id", playerId);
		try {
			mOracleHandler.runUpdate(sql);
			// System.out.println("Strike Rotated");
			// Tools.pToast("Strike Rotated");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}

		mOracleHandler.close();
	}

	protected void updateBowlingTable(String bowlerId) {
		// TODO Auto-generated method stub

		
		String tableName= cbowlTable;
		if (rbOk.isSelected()) {
			incAFieldByPlayerId(tableName, "cov_ball", bowlerId);
		}

		if (rbNb.isSelected()) {
			incAFieldByPlayerId(tableName, "nb", bowlerId);
		}
		if (rbWd.isSelected()) {
			incAFieldByPlayerId(tableName, "wd", bowlerId);
		}

		// if(rbLb.isSelected()){
		// incAFieldByPlayerId(tableName,"lb",bowlerId);
		// }

		if (rbBat.isSelected()) {

			int n = (int) spBatLbRun.getValue();
			//n=4;
			incAFieldByPlayerId(tableName, "s" + n, bowlerId);
		}

		if (rbBowled.isSelected() || rbCatch.isSelected() || rbStamping.isSelected()) {
			incAFieldByPlayerId(tableName, "wk", bowlerId);
		}

	}

	protected void updateBattingTable(String playerId) {
		// TODO Auto-generated method stub
		
		String tableName= cbatTable;
		if (rbBat.isSelected()) {
			int n = (int) spBatLbRun.getValue();
			incAFieldByPlayerId(cbatTable, "s" + n, playerId);
		}
		
		if (rbBowled.isSelected() || rbCatch.isSelected() || rbStamping.isSelected()) {
			//incAFieldByPlayerId(tableName, "wk", playerId);
			updateAFieldByPlayerId(tableName, "is_out", ""+1, playerId);
			updateAFieldByPlayerId(tableName, "on_str", ""+0, playerId);
			
		}
		
		

	}

	protected void incAField(String tableName, String col, String conCol, String conValue) {
		// TODO Auto-generated method stub

		String s = getAStringValueFromDB(tableName, col, conCol, conValue);

		int oldValue = Integer.valueOf(s);

		String newValue = String.valueOf(oldValue + 1);
		
		updateAField(tableName, col, newValue, conCol, conValue);

	}
	protected void incAFieldByPlayerId(String tableName, String col, String playerId) {
		// TODO Auto-generated method stub
		
		String s = getAStringValueFromDB(tableName, col, "player_id", playerId);
		
		int oldValue = Integer.valueOf(s);
		
		mOracleHandler.openDatabase();
		
		String newValue = String.valueOf(oldValue + 1);
		
		String sql = SqlHelper.updateTable(tableName, col, newValue, "player_id", playerId);
		try {
			mOracleHandler.runUpdate(sql);
			// System.out.println("Strike Rotated");
			// Tools.pToast("Strike Rotated");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		
		mOracleHandler.close();
		
	}

	protected String getStrikePlayer(String tableName) {

		String s = getAStringValueFromDB(tableName, "player_id", "on_str", 1);

		return s;
	}

	protected String getAStringValueFromDB(String tableName, String col, String conCol, String conValue) {
		// TODO Auto-generated method stub
		mOracleHandler.openDatabase();

		ResultSet rs = null;

		String sql = SqlHelper.getACell(tableName, col, conCol, conValue);
		try {
			rs = mOracleHandler.runQuery(sql);
			// System.out.println("Strike Rotated");
			// Tools.pToast("Strike Rotated");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}

		String s = RSHelper.getString(rs);
		mOracleHandler.close();

		return s;
	}

	protected String getAStringValueFromDB(String tableName, String col, String conCol, int conValue) {
		// TODO Auto-generated method stub
		mOracleHandler.openDatabase();

		ResultSet rs = null;

		String sql = SqlHelper.getACell(tableName, col, conCol, conValue);
		try {
			rs = mOracleHandler.runQuery(sql);
			// System.out.println("Strike Rotated");
			// Tools.pToast("Strike Rotated");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}

		String s = RSHelper.getString(rs);
		mOracleHandler.close();

		return s;
	}

	protected void setStrike(String tableName, int pos, int value) {
		// TODO Auto-generated method stub

		mOracleHandler.openDatabase();

		String sql = SqlHelper.updateTable(tableName, "on_str", "" + value, "pos", pos);
		try {
			mOracleHandler.runUpdate(sql);
			System.out.println("Strike Rotated");
			// Tools.pToast("Strike Rotated");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}

		mOracleHandler.close();

	}

	protected void addToBatting(String playerId) {
		// TODO Auto-generated method stub

		int pos = getTotalTuples(cbatTable) + 1;

		mOracleHandler.openDatabase();

		String[] col = { "pos", "player_id" };
		String[] arg = { "" + pos, playerId };

		String sql = SqlHelper.insertValueByColArg(cbatTable, col, arg);
		System.out.println(sql);
		try {
			mOracleHandler.runUpdate(sql);
			Tools.pToast("added");
			System.out.println("Player added");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			System.err.println("Player Not added");
		}

		mOracleHandler.close();

	}

	protected void addToBowling(String playerId) {
		// TODO Auto-generated method stub

		int pos = getTotalTuples(cbowlTable) + 1;

		mOracleHandler.openDatabase();

		String[] col = { "pos", "player_id" };
		String[] arg = { "" + pos, playerId };

		String sql = SqlHelper.insertValueByColArg(cbowlTable, col, arg);
		System.out.println(sql);
		try {
			mOracleHandler.runUpdate(sql);
			Tools.pToast("added");
			System.out.println("Player added");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			System.err.println("Player Not added");
		}

		mOracleHandler.close();

	}

	private int getTotalTuples(String tableName) {
		// TODO Auto-generated method stub
		mOracleHandler.openDatabase();

		ResultSet rs = null;

		try {
			rs = mOracleHandler.runQuery(SqlHelper.getATable(tableName));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}

		int n = RSHelper.getTotalResult(rs);

		mOracleHandler.close();

		return n;
	}

	protected void refreshTables() {
		// TODO Auto-generated method stub
		setTableSquad();
		setTableBatting();
		setTableBowling();
	}

	protected void enableAllRun(boolean b) {
		// TODO Auto-generated method stub
		rbBat.setEnabled(b);
		//rbLb.setEnabled(b);
		spBatLbRun.setEnabled(b);

		// chbBye.setEnabled(b);
		// spBye.setEnabled(b);

	}

	protected void enableAllOut(boolean b) {
		// TODO Auto-generated method stub
		rbBowled.setEnabled(b);
		rbCatch.setEnabled(b);
		rbRunOut.setEnabled(b);
		rbStamping.setEnabled(b);

	}

	protected void setAllRun(boolean b) {
		// TODO Auto-generated method stub
		rbBat.setSelected(b);
		rbLb.setSelected(b);
		// spBatLb.setSelected(b);

		// chbBye.setEnabled(b);
		// spBye.setEnabled(b);

	}

	protected void setAllOut(boolean b) {
		// TODO Auto-generated method stub
		rbBowled.setSelected(b);
		rbCatch.setSelected(b);
		rbRunOut.setSelected(b);
		rbStamping.setSelected(b);

	}

	protected void refreshTask() {
		// TODO Auto-generated method stub
		String matchStatus = getMatchStatusFromDB();

		System.out.println(matchStatus);
		lblMatchStatus.setText(matchStatus);

		// setTableSquad();
		// setTableBatting();
		// setTableBowling();

	}

	private void setTableSquad() {
		// TODO Auto-generated method stub

		getMatchStatusFromDB();

		String teamId = null;
		if (cmbIng.getSelectedIndex() == 0 && cmbBatBowl.getSelectedIndex() == 0) {
			teamId = inn1BatTeamId;
		} else if (cmbIng.getSelectedIndex() == 0 && cmbBatBowl.getSelectedIndex() == 1) {
			teamId = inn1BowlTeamId;
		} else if (cmbIng.getSelectedIndex() == 1 && cmbBatBowl.getSelectedIndex() == 0) {

			teamId = inn2BatTeamId;

		} else if (cmbIng.getSelectedIndex() == 1 && cmbBatBowl.getSelectedIndex() == 1) {
			teamId = inn2BowlTeamId;
		}

		Tools.pToast(teamId);

		String tableName = DataUtils.TABLE_FINAL_SQUAD_PREFIX + matchId;

		mOracleHandler.openDatabase();

		String sql = SqlHelper.getFinalSquadByTeam(tableName, teamId, "player_id");
		// System.out.println(sql);
		ResultSet rs = null;
		try {
			rs = mOracleHandler.runQuery(sql);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println(tableName + " runQuery failed");
			// Tools.pToast(tableName+" runQuery failed");
			return;
		}

		tableSquad.setModel(DbUtils.resultSetToTableModel(rs));
		mOracleHandler.close();
	}

	private void setTableBatting() {
		// TODO Auto-generated method stub

		// getMatchStatusFromDB();

		String tableName = cbatTable;

		mOracleHandler.openDatabase();

		ResultSet rs = null;
		try {
			rs = mOracleHandler.runQuery(SqlHelper.getATable(tableName,"is_out"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println(tableName + " runQuery failed");
			// Tools.pToast(tableName+" runQuery failed");
			return;
		}

		tableBatting.setModel(DbUtils.resultSetToTableModel(rs));
		mOracleHandler.close();
	}

	private void setTableBowling() {
		// TODO Auto-generated method stub

		// getMatchStatusFromDB();

		String tableName = cbowlTable;

		mOracleHandler.openDatabase();

		ResultSet rs = null;
		try {
			rs = mOracleHandler.runQuery(SqlHelper.getATable(tableName));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println(tableName + " runQuery failed");
			// Tools.pToast(tableName+" runQuery failed");
			return;
		}

		tableBowling.setModel(DbUtils.resultSetToTableModel(rs));
		mOracleHandler.close();
	}

	private String getMatchStatusFromDB() {
		// TODO Auto-generated method stub

		mOracleHandler.openDatabase();
		String sql = null;
		String tableName = DataUtils.TABLE_INNINGS;
		ResultSet rs = null;

		String bat1 = null, bat2 = null, bowl1 = null, bowl2 = null;

		try {
			sql = SqlHelper.getACell(tableName, "inn1_bat", "match_id", matchId);
			rs = mOracleHandler.runQuery(sql);
			bat1 = RSHelper.getString(rs);

			sql = SqlHelper.getACell(tableName, "inn1_bowl", "match_id", matchId);
			rs = mOracleHandler.runQuery(sql);
			bowl1 = RSHelper.getString(rs);

			sql = SqlHelper.getACell(tableName, "inn2_bat", "match_id", matchId);
			rs = mOracleHandler.runQuery(sql);
			bat2 = RSHelper.getString(rs);

			sql = SqlHelper.getACell(tableName, "inn2_bowl", "match_id", matchId);
			rs = mOracleHandler.runQuery(sql);
			bowl2 = RSHelper.getString(rs);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println("Get Match Status Failed");
		}

		mOracleHandler.close();

		inn1BatTeamId = bat1;
		inn1BowlTeamId = bowl1;
		inn2BatTeamId = bat2;
		inn2BowlTeamId = bowl2;

		String inningsStatus = "Inn1Bat: " + bat1 + "\nInn1Bowl: " + bowl1 + "\nInn2Bat: " + bat2 + "\nInn2Bowl: "
				+ bowl2;

		System.out.println(inningsStatus);

		int innNo = cmbIng.getSelectedIndex() + 1;
		// String curBattingTeamId, curBowlingTeamId;
		if (innNo == 1) {
			curBattingTeamId = bat1;
			curBowlingTeamId = bowl1;
		} else {
			curBattingTeamId = bat2;
			curBowlingTeamId = bowl2;
		}

		String curMatchStatus = "Batting: " + curBattingTeamId + "  Bowling: " + curBowlingTeamId;

		cbatTable = matchId + DataUtils.TABLE_BATTING_INFIX + curBattingTeamId;
		cbowlTable = matchId + DataUtils.TABLE_BOWLING_INFIX + curBowlingTeamId;

		System.out.println(cbatTable + "\n" + cbowlTable);

		return curMatchStatus;

	}


	private void refreshScoreField() {
		// TODO Auto-generated method stub

		String curBattingCode, curBowlingCode;

		curBattingCode = getTeamCodeFromId(curBattingTeamId);
		curBowlingCode = getTeamCodeFromId(curBowlingTeamId);

		int run = getTotalRun();
		int wicket = getTotalWicket();
		double over = getTotalOver();
		double rr = 0.0;

		if (over > 0.0) {
			rr = run / (over);
		}

		String scoreText = curBattingCode.toUpperCase() + " " + run + "/" + wicket + " (" + over + " ov) ";
		lblScore.setText(scoreText);
		lblRunRate.setText("RR " + rr);

		String bowlingText = curBowlingCode.toUpperCase() + " Bowling";
		lblBowling.setText(bowlingText);

	}

	private double getTotalOver() {
		// TODO Auto-generated method stub
		mOracleHandler.openDatabase();
		String sql = "select sum(over) as tot_ov from " + cbowlTable;
		
		ResultSet rs = null;
		
		try {
			rs = mOracleHandler.runQuery(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		//double d = Double.parseDouble(RSHelper.getString(rs));
		
		int over = Integer.valueOf(RSHelper.getString(rs));
		sql = "select sum(cov_ball) as tot_ov from " + cbowlTable;
		
		try {
			rs = mOracleHandler.runQuery(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		int ball =  Integer.valueOf(RSHelper.getString(rs));
		
		over = over + ball/6;
		ball = ball%6;
		
		String s = over+"."+ball;
		
		mOracleHandler.close();
		
		double d = Double.valueOf(s);
		
		//double d=0.0;
		return d;
	}

	private int getTotalWicket() {
		// TODO Auto-generated method stub
		
		mOracleHandler.openDatabase();
		String sql = "select sum(wk) as tot_wk from " + cbowlTable;
		
		ResultSet rs = null;
		
		int n=0;
		try {
			rs = mOracleHandler.runQuery(sql);
			n = Integer.valueOf(RSHelper.getString(rs));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			n=0;
		}
		
//		n = Integer.valueOf(RSHelper.getString(rs));
		
		mOracleHandler.close();
		
		
		return n;
	}

	private int getTotalRun() {
		// TODO Auto-generated method stub
		mOracleHandler.openDatabase();
		String sql = "select sum(run) as tot_run from " + cbowlTable;
		
		ResultSet rs = null;
		
		int n=0;
		try {
			rs = mOracleHandler.runQuery(sql);
			n = Integer.valueOf(RSHelper.getString(rs));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			n=0;
		}
		
		//n = Integer.valueOf(RSHelper.getString(rs));
		
		
		
		mOracleHandler.close();
		return n;
	}

	private void wipeTable(String tableName) {
		mOracleHandler.openDatabase();
		try {
			mOracleHandler.runUpdate(SqlHelper.deleteAllTuples(tableName));

			System.out.println(tableName + " Table wiped");
			// Tools.pToast("Table Created");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			System.err.println(tableName + "Table not wiped/may not be exist");
			// Tools.pToast("Table not Created/may be exist");
		}

		mOracleHandler.close();
	}

	private String getTeamCodeFromId(String teamId) {

		mOracleHandler.openDatabase();
		ResultSet rs = null;
		try {
			rs = mOracleHandler
					.runQuery(SqlHelper.getACell(DataUtils.TABLE_TOURNAMENT_TEAMS, "team_code", "team_id", teamId));

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();

		}
		String s = RSHelper.getString(rs);
		mOracleHandler.close();

		return s;
	}

	private String getTeamNameFromId(String teamId) {

		mOracleHandler.openDatabase();
		ResultSet rs = null;
		try {
			rs = mOracleHandler
					.runQuery(SqlHelper.getACell(DataUtils.TABLE_TOURNAMENT_TEAMS, "team_name", "team_id", teamId));

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();

		}
		String s = RSHelper.getString(rs);
		mOracleHandler.close();

		return s;
	}
}
