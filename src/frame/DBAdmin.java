package frame;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import database.oracle.OracleHandler;
import utils.DataUtils;
import utils.SqlHelper;
import utils.Tools;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class DBAdmin extends JFrame {

	private JPanel contentPane;
	OracleHandler mOracleHandler = new OracleHandler();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DBAdmin frame = new DBAdmin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DBAdmin() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 585, 466);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnInitializeAllSchema = new JButton("Initialize all Schema");
		btnInitializeAllSchema.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				//on del cascade on team
				createTable(DataUtils.TABLE_TOURNAMENT_TEAMS);
				createTable(DataUtils.TABLE_POINT);
				createTable(DataUtils.TABLE_PLAYERS);
				
				//on del cascade on match
				createTable(DataUtils.TABLE_MATCH);
				createTable(DataUtils.TABLE_TOSS);
				createTable(DataUtils.TABLE_INNINGS);
				createTable(DataUtils.TABLE_RESULT);
				
						
				
			}
		});
		btnInitializeAllSchema.setBounds(223, 72, 153, 23);
		contentPane.add(btnInitializeAllSchema);
		
		JButton btnWipeTables = new JButton("Wipe All Tables");
		btnWipeTables.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				mOracleHandler.openDatabase();
				ArrayList<String> al = mOracleHandler.getUserTables();
				mOracleHandler.close();
				
				for (int i = 0; i <al.size(); i++) {
					wipeTable(al.get(i));
				}
			}
		});
		btnWipeTables.setBounds(223, 106, 153, 23);
		contentPane.add(btnWipeTables);
		
		JButton btnDropAllTables = new JButton("Drop All Tables");
		btnDropAllTables.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mOracleHandler.openDatabase();
				ArrayList<String> al = mOracleHandler.getUserTables();
				mOracleHandler.close();
				
				for (int i = 0; i <al.size(); i++) {
					dropTable(al.get(i));
				}
				
			}
		});
		btnDropAllTables.setBounds(223, 142, 153, 23);
		contentPane.add(btnDropAllTables);
	}
	
	
	private void createTable(String tableName){
		mOracleHandler.openDatabase();
		try {
			mOracleHandler.runUpdate(SqlHelper.createTable(tableName));
			
			System.out.println(tableName+ " Table Created");
			//Tools.pToast("Table Created");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.err.println(tableName + "Table not Created/may be exist");
			//Tools.pToast("Table not Created/may be exist");
		}
		
		mOracleHandler.close();
	}
	
	
	private void dropTable(String tableName){
		mOracleHandler.openDatabase();
		try {
			mOracleHandler.runUpdate(SqlHelper.dropTable(tableName));
			
			System.out.println(tableName+ " Table dropped");
			//Tools.pToast("Table Created");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.err.println(tableName + "Table not dropped/may not be exist");
			//Tools.pToast("Table not Created/may be exist");
		}
		
		mOracleHandler.close();
	}
	private void wipeTable(String tableName){
		mOracleHandler.openDatabase();
		try {
			mOracleHandler.runUpdate(SqlHelper.deleteAllTuples(tableName));
			
			System.out.println(tableName+ " Table wiped");
			//Tools.pToast("Table Created");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.err.println(tableName + "Table not wiped/may not be exist");
			//Tools.pToast("Table not Created/may be exist");
		}
		
		mOracleHandler.close();
	}
}
