package frame;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import database.oracle.OracleHandler;
import net.proteanit.sql.DbUtils;
import utils.DataUtils;
import utils.PdfHelper;
import utils.SqlHelper;
import utils.TimeHelper;
import utils.Tools;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class ReportGenarator extends JFrame {

	private JPanel contentPane;
	private JTextField tfQuery;
	private JTextField tfReportTitle;
	private JTable table;
	
	String repGenTime;
	String reportTitle;
	
	JLabel lblTime;
	
	ResultSet rs = null;
	ResultSet rsPrint = null;
	
	
	
	OracleHandler mOracleHandler = new OracleHandler();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ReportGenarator frame = new ReportGenarator();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ReportGenarator() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 808, 501);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnOption = new JMenu("Option");
		menuBar.add(mnOption);
		
		JMenuItem mntmAllTournamentPlayers = new JMenuItem("All Tournament Players");
		mntmAllTournamentPlayers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				setTable(DataUtils.TABLE_PLAYERS);
			}
		});
		mnOption.add(mntmAllTournamentPlayers);
		
		JMenuItem mntmTournamentTeams = new JMenuItem("Tournament Teams");
		mntmTournamentTeams.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setTable(DataUtils.TABLE_TOURNAMENT_TEAMS);
			}
		});
		mnOption.add(mntmTournamentTeams);
		
		JMenuItem mntmToss = new JMenuItem("Toss");
		mntmToss.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setTable(DataUtils.TABLE_TOSS);
			}
		});
		mnOption.add(mntmToss);
		
		JMenuItem mntmResult = new JMenuItem("Result");
		mntmResult.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setTable(DataUtils.TABLE_RESULT);
			}
		});
		mnOption.add(mntmResult);
		
		JMenuItem mntmPointTable = new JMenuItem("Point Table");
		mntmPointTable.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				
				setTable(DataUtils.TABLE_POINT);
			}
			
		});
		mnOption.add(mntmPointTable);
		
		JMenuItem mntmPrint = new JMenuItem("Print");
		mnOption.add(mntmPrint);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblEnterQuery = new JLabel("Enter Query");
		lblEnterQuery.setBounds(10, 11, 67, 14);
		contentPane.add(lblEnterQuery);
		
		tfQuery = new JTextField();
		tfQuery.setText("select * from player");
		tfQuery.setBounds(87, 8, 618, 20);
		contentPane.add(tfQuery);
		tfQuery.setColumns(10);
		
		JButton btnRun = new JButton("Run");
		btnRun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				reportGenaratedTimeSet();
				
				//mOracleHandler.openDatabase();
				
				String sql = tfQuery.getText().replace(';', ' ').trim();
				
				reportTitle = sql;
				//ResultSet rs = null;
				try {
					rs = mOracleHandler.runQuery(sql,true);
					
					//rs = mOracleHandler.runQuery(SqlHelper.getATable("all_cric_teams", "team_id"));
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				//PdfHelper.createPdfFromResultSet(rs, Tools.makeFileNameFromCurrentTime(), "title");
			
				
				if(rs!=null){
					setRSToTable(rs);
				}
				
				//mOracleHandler.close();
			}
		});
		btnRun.setBounds(715, 7, 67, 23);
		contentPane.add(btnRun);
		
		tfReportTitle = new JTextField();
		tfReportTitle.setBounds(87, 39, 618, 20);
		contentPane.add(tfReportTitle);
		tfReportTitle.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 70, 772, 327);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JLabel lblTitle = new JLabel("Title");
		lblTitle.setBounds(10, 42, 46, 14);
		contentPane.add(lblTitle);
		
		JButton btnView = new JButton("View");
		btnView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnView.setBounds(715, 38, 67, 23);
		contentPane.add(btnView);
		
		JButton btnPrint = new JButton("Print");
		btnPrint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//mOracleHandler.openDatabase();
				String temp = repGenTime;
				temp = temp.replace(':','_');
				temp = temp.replace('/','_');
				//temp = temp.replace(' ','_');
				
				String fileName  = "Report " + temp +".pdf";
				
				moveResultSetToFirst();
				
				reportTitle = tfReportTitle.getText();
				try {
					PdfHelper.createPdfFromResultSet(rs, fileName, reportTitle);
					System.out.println(fileName + " Report Created Successfully");
					Tools.pToast(fileName + " Report Created Successfully");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					System.err.println(fileName + " Report Creation faliled");
					//e1.printStackTrace();
				}
				
				//mOracleHandler.close();
			}
		});
		btnPrint.setBounds(10, 408, 89, 23);
		contentPane.add(btnPrint);
		
		JButton btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnRefresh.setBounds(109, 408, 89, 23);
		contentPane.add(btnRefresh);
		
		lblTime = new JLabel("Time");
		lblTime.setHorizontalAlignment(SwingConstants.TRAILING);
		lblTime.setBounds(576, 417, 206, 14);
		contentPane.add(lblTime);
		
		firstView();
	}

	protected void moveResultSetToFirst() {
		// TODO Auto-generated method stub
		try {
			rs.beforeFirst();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	protected void reportGenaratedTimeSet() {
		// TODO Auto-generated method stub
		
		repGenTime = TimeHelper.getCurrentTimeDate();
		lblTime.setText("RGT: " +repGenTime);
		
		
	}
	
	

	private void firstView() {
		// TODO Auto-generated method stub
		//TimeHelper.get
		
		lblTime.setText("CT: "+ TimeHelper.getCurrentTimeDate());
		mOracleHandler.openDatabase();
	}
	
	private void setRSToTable(ResultSet rs) {
		// TODO Auto-generated method stub
		table.setModel(DbUtils.resultSetToTableModel(rs));
	}
	
	private void setTable(String tableName) {
		// TODO Auto-generated method stub
		
		if(tableName==null){
			System.err.println(tableName+" tableName is null in setTtable func");			
			return;
		}
		//mOracleHandler.openDatabase();
		
		//ResultSet rs = null;
		try {
			rs = mOracleHandler.runQuery(SqlHelper.getATable(tableName),true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println(tableName+" runQuery failed");
			Tools.pToast(tableName+" runQuery failed");
			return;
		}
		
		table.setModel(DbUtils.resultSetToTableModel(rs));
		//mOracleHandler.close();
		reportGenaratedTimeSet();
		
	}
}
