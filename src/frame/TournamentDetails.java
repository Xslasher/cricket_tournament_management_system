package frame;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import database.oracle.OracleHandler;
import database.sqlite.SqliteHandler;
import model.Team;
import model.TournamentTeam;
import net.proteanit.sql.DbUtils;
import utils.DataUtils;
import utils.PrefUtils;
import utils.RSHelper;
import utils.SqlHelper;
import utils.Tools;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class TournamentDetails extends JFrame {

	private JPanel contentPane;
	private JTextField tfTournamentTitle;
	private JTextField tfHost;
	private JTextField tfTotalTeams;
	private JTable table;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JRadioButton rbTest, rbOdi, rbT20;
	JComboBox cmbSampleTeam;

	// private JTextField tfTeamId;
	// private JTextField tfTeamName;

	OracleHandler mOracleHandler = new OracleHandler();
	ArrayList<Team> allTeams = new ArrayList<>();

	private int pos = 0;
	private JButton btnDeleteTeam;
	private JPanel panel_1;
	private JTextField tfTeamIdPrefix;
	private JTextField tfTeamId;
	private JTextField tfTeamName;
	private JTextField tfTeamCode;
	String selTeamIdToRemove;
	private JLabel lblSampleTeam;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TournamentDetails frame = new TournamentDetails();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TournamentDetails() {
		setTitle(DataUtils.FRAME_TITLE_TOURNAMENT_DETAILS);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 684, 495);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();

		scrollPane.setBounds(10, 159, 648, 147);
		contentPane.add(scrollPane);

		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row = table.getSelectedRow();
				selTeamIdToRemove = table.getModel().getValueAt(row, 0).toString();
				Tools.pToast(selTeamIdToRemove);
			}
		});
		scrollPane.setViewportView(table);

		JButton btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// int
				// totalTeams=PrefUtils.getInt(PrefUtils.TOURNAMENT_TOTAL_TEAMS,
				// 0);
				// tfTotalTeams.setText(String.valueOf(totalTeams));

				tfTotalTeams.setText(String.valueOf(getTotalTeams()));

				setTable(DataUtils.TABLE_TOURNAMENT_TEAMS);
			}
		});
		btnRefresh.setBounds(328, 317, 89, 23);
		contentPane.add(btnRefresh);

		btnDeleteTeam = new JButton("Delete Team");
		btnDeleteTeam.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				removeFromTournamentTeam(DataUtils.TABLE_TOURNAMENT_TEAMS, selTeamIdToRemove);
				tfTotalTeams.setText(String.valueOf(getTotalTeams()));
				setTable(DataUtils.TABLE_TOURNAMENT_TEAMS);

				// if (flag) {
				// // removePlayerByTeam(team);
				// }

			}
		});
		btnDeleteTeam.setBounds(217, 317, 101, 23);
		contentPane.add(btnDeleteTeam);

		JLabel lblTournamentTeams = new JLabel("Team List");
		lblTournamentTeams.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTournamentTeams.setHorizontalAlignment(SwingConstants.CENTER);
		lblTournamentTeams.setBounds(252, 129, 142, 19);
		contentPane.add(lblTournamentTeams);

		JPanel panel = new JPanel();
		panel.setBounds(48, 11, 583, 107);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblNewLabel = new JLabel("Tournament Title");
		lblNewLabel.setBounds(10, 11, 110, 17);
		panel.add(lblNewLabel);

		JLabel lblHost = new JLabel("Host");
		lblHost.setBounds(10, 39, 94, 20);
		panel.add(lblHost);

		JLabel lblMatchType = new JLabel("Match Type");
		lblMatchType.setBounds(10, 70, 94, 19);
		panel.add(lblMatchType);

		tfTournamentTitle = new JTextField();
		tfTournamentTitle.setBounds(130, 9, 276, 19);
		panel.add(tfTournamentTitle);
		tfTournamentTitle.setColumns(10);

		tfHost = new JTextField();
		tfHost.setBounds(130, 39, 127, 19);
		panel.add(tfHost);
		tfHost.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("Total Teams");
		lblNewLabel_1.setBounds(267, 41, 73, 17);
		panel.add(lblNewLabel_1);

		tfTotalTeams = new JTextField();
		tfTotalTeams.setBounds(363, 39, 43, 19);
		panel.add(tfTotalTeams);
		tfTotalTeams.setEditable(false);
		tfTotalTeams.setText("0");
		tfTotalTeams.setColumns(10);

		rbTest = new JRadioButton("Test");
		rbTest.setBounds(130, 68, 87, 23);
		panel.add(rbTest);
		rbTest.setEnabled(false);
		rbTest.setActionCommand("rbTest");
		buttonGroup.add(rbTest);

		rbOdi = new JRadioButton("ODI");
		rbOdi.setBounds(219, 69, 87, 21);
		panel.add(rbOdi);
		rbOdi.setActionCommand("rbOdi");
		buttonGroup.add(rbOdi);

		rbT20 = new JRadioButton("T20");
		rbT20.setBounds(308, 69, 67, 20);
		panel.add(rbT20);
		rbT20.setActionCommand("rbT20");
		buttonGroup.add(rbT20);

		JButton btnSave = new JButton("Save");
		btnSave.setBounds(448, 11, 87, 42);
		panel.add(btnSave);

		panel_1 = new JPanel();
		panel_1.setBounds(10, 366, 648, 80);
		contentPane.add(panel_1);
		panel_1.setLayout(null);

		JLabel lblTeamId = new JLabel("Team Id");
		lblTeamId.setBounds(10, 11, 46, 14);
		panel_1.add(lblTeamId);

		tfTeamIdPrefix = new JTextField();
		tfTeamIdPrefix.setEditable(false);
		tfTeamIdPrefix.setText("ict");
		tfTeamIdPrefix.setBounds(66, 8, 39, 20);
		panel_1.add(tfTeamIdPrefix);
		tfTeamIdPrefix.setColumns(10);

		tfTeamId = new JTextField();
		tfTeamId.setToolTipText("number");
		tfTeamId.setBounds(115, 8, 60, 20);
		panel_1.add(tfTeamId);
		tfTeamId.setColumns(10);

		cmbSampleTeam = new JComboBox();
		cmbSampleTeam.setBounds(277, 52, 148, 20);
		panel_1.add(cmbSampleTeam);

		tfTeamName = new JTextField();
		tfTeamName.setBounds(241, 8, 127, 20);
		panel_1.add(tfTeamName);
		tfTeamName.setColumns(10);

		JLabel lblTeamName = new JLabel("Name");
		lblTeamName.setBounds(185, 11, 46, 14);
		panel_1.add(lblTeamName);

		JLabel lblNewLabel_2 = new JLabel("Code");
		lblNewLabel_2.setBounds(378, 11, 46, 14);
		panel_1.add(lblNewLabel_2);

		tfTeamCode = new JTextField();
		tfTeamCode.setBounds(434, 8, 86, 20);
		panel_1.add(tfTeamCode);
		tfTeamCode.setColumns(10);

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String teamId, teamName, teamCode;
				teamId = tfTeamId.getText().toString().trim();
				teamName = tfTeamName.getText().toString().trim();
				teamCode = tfTeamCode.getText().toString().trim();

				// pToast(teamId+teamName+teamCode);

				if (teamId == null || teamName == null || teamCode == null) {
					Tools.pToast("Enter valid data");
					return;
				} else if (teamId.length() < 1 || teamName.length() < 1 || teamCode.length() < 1) {
					Tools.pToast("Enter valid data");
					return;
				}

				teamId = "ict" + teamId;
				String[] arr = { teamId, teamName, teamCode };

				mOracleHandler.openDatabase();
				// mOracleHandler.insertData("all_cric_teams", "t", "t", "t");

				try {

					mOracleHandler.runUpdate(SqlHelper.insertValueByColArg(DataUtils.TABLE_TOURNAMENT_TEAMS,
							DataUtils.COLIS_TOURNAMENT_TEAM, arr));
					
					mOracleHandler.close();
					addToPointTable(teamId);
					
					tfTotalTeams.setText(String.valueOf(getTotalTeams()));	//close database
					Tools.pToast("data inserted");
				} catch (SQLException e2) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
					Tools.pToast("data not inserted");
					mOracleHandler.close();
				}

				//mOracleHandler.close();
				setTable(DataUtils.TABLE_TOURNAMENT_TEAMS);
			}
		});
		btnAdd.setBounds(549, 7, 89, 23);
		panel_1.add(btnAdd);

		lblSampleTeam = new JLabel("Sample Team");
		lblSampleTeam.setBounds(185, 55, 82, 14);
		panel_1.add(lblSampleTeam);
		cmbSampleTeam.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				JComboBox cmb = (JComboBox) e.getSource();
				// String s = (String) cmb.getSelectedItem();
				// String s=e.getItem().toString(); //working wrong
				pos = cmb.getSelectedIndex();

				//Tools.pToast(allTeams.get(pos).toString());

				String teamId = allTeams.get(pos).getTeamId().replace("ict", "");
				String teamName = allTeams.get(pos).getTeamName();
				String teamCode = allTeams.get(pos).getTeamCode();
				tfTeamId.setText(teamId);
				tfTeamName.setText(teamName);
				tfTeamCode.setText(teamCode);

			}
		});
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String title, host, matchType;
				// int totalTeams;

				title = tfTournamentTitle.getText().toString();
				host = tfHost.getText().toString();
				// totalTeams =
				// Integer.valueOf(tfTotalTeams.getText().toString());

				// ButtonModel rb = buttonGroup.getSelection().;
				matchType = getMatchTypeFromRB();

				PrefUtils.putString(PrefUtils.TOURNAMENT_TITLE, title);
				PrefUtils.putString(PrefUtils.TOURNAMENT_HOST, host);
				PrefUtils.putString(PrefUtils.TOURNAMENT_MATCH_TYPE, matchType);

			}
		});

		firstView();
	}

	protected void addToPointTable(String teamId) {
		// TODO Auto-generated method stub
		
		mOracleHandler.openDatabase();
		
		String[] col = {"team_id"};
		String[] arg = {teamId};
		
		String sql=null;
		sql = SqlHelper.insertValueByColArg(DataUtils.TABLE_POINT, col, arg);
		
		try {
			mOracleHandler.runUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		mOracleHandler.close();
	}

	private void firstView() {
		// TODO Auto-generated method stub
		tfTournamentTitle.setText(PrefUtils.getString(PrefUtils.TOURNAMENT_TITLE, ""));
		tfHost.setText(PrefUtils.getString(PrefUtils.TOURNAMENT_HOST, ""));
		// tfTotalTeams.setText(PrefUtils.getString(PrefUtils.TOURNAMENT_TOTAL_TEAMS,
		// ""));
		tfTotalTeams.setText(String.valueOf(getTotalTeams()));

		String matchType = PrefUtils.getString(PrefUtils.TOURNAMENT_MATCH_TYPE, "");

		if (matchType.equalsIgnoreCase(DataUtils.MATCH_ODI)) {
			rbOdi.setSelected(true);
		} else if (matchType.equalsIgnoreCase(DataUtils.MATCH_T20)) {
			rbT20.setSelected(true);
		} else if (matchType.equalsIgnoreCase(DataUtils.MATCH_TEST)) {
			rbTest.setSelected(true);
		}

		// rbT20.setSelected(true);

		// buttonGroup.setSelected(rb, true);
		 setTable(DataUtils.TABLE_TOURNAMENT_TEAMS);

		AddAllTeamsToComboBox();
		if (cmbSampleTeam.getItemCount() > 0) {
			cmbSampleTeam.setSelectedIndex(0);
		}

	}

	protected void removeFromTournamentTeam(String tableName, String teamId) {

		mOracleHandler.openDatabase();

		try {
			mOracleHandler.runUpdate(SqlHelper.deleteTuple(tableName, "team_id", teamId));
			//Tools.pToast("added");
			System.out.println("Team Deleted");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			System.err.println("Team not Deleted");
		}

		mOracleHandler.close();

	}

	protected String getMatchTypeFromRB() {
		// TODO Auto-generated method stub

		String ac = buttonGroup.getSelection().getActionCommand().toString();
		String matchType = null;

		switch (ac) {
		case "rbTest":
			matchType = DataUtils.MATCH_TEST;
			break;
		case "rbOdi":
			matchType = DataUtils.MATCH_ODI;
			break;
		case "rbT20":
			matchType = DataUtils.MATCH_T20;
			break;

		default:
			break;
		}
		return matchType;
	}

	protected void removePlayerByTeam(Team team) {
		// TODO Auto-generated method stub

		String sql = null;

		mOracleHandler.openDatabase();

		sql = SqlHelper.deleteTuple(DataUtils.TABLE_SQUADS, "team_id", team.getTeamId());
		try {
			mOracleHandler.runUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			Tools.pToast("Player not deleted or not exist");
		}

		String pattern = "%" + team.getTeamCode() + "%";
		sql = SqlHelper.deleteTupleLike(DataUtils.TABLE_PLAYERS, "player_id", pattern);
		try {
			mOracleHandler.runUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			Tools.pToast("Player not deleted or not exist");
		}

		mOracleHandler.close();

	}

	private void AddAllTeamsToComboBox() {
		// TODO Auto-generated method stub

		// ArrayList<Team> al = new ArrayList<>();
		allTeams = getAllTeam();

		for (int i = 0; i < allTeams.size(); i++) {
			//System.out.println(allTeams.get(i).getTeamName());
			cmbSampleTeam.addItem(allTeams.get(i).getTeamName());
			// cmbSampleTeam.addItem("Hello");
		}

	}

	protected ArrayList<Team> getAllTeam() {
		// TODO Auto-generated method stub

		ResultSet rs = null;
		ArrayList<Team> al = new ArrayList<>();

		SqliteHandler mSqliteHandler = new SqliteHandler();
		try {
			mSqliteHandler.openDatabase(DataUtils.SQLITE_DB_NAME);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		try {
			rs = mSqliteHandler.runQuery(SqlHelper.getATable("all_cric_teams"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		al = RSHelper.getAllTeamBase(rs);
		mSqliteHandler.close();

		for (int i = 0; i < al.size(); i++) {
			Team team = new Team();
			team = al.get(i);
			// cmbSampleTeam.addItem("hello");
			//System.out.println("getAllTeam() " + team.toString());
		}

		return al;
	}

	private void setTable(String tableName) {
		// TODO Auto-generated method stub

		mOracleHandler.openDatabase();
		if (tableName == null) {
			System.err.println(tableName + " tableName is null in setTtable func");
			return;
		}

		ResultSet rs = null;
		try {
			rs = mOracleHandler.runQuery(SqlHelper.getATable(tableName, "team_id"));
			// rs =
			// mOracleHandler.runQuery(SqlHelper.getTournamentTeams("team_id"));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			System.err.println(tableName + " runQuery failed");
			// Tools.pToast(tableName + " runQuery failed");
			return;
		}

		table.setModel(DbUtils.resultSetToTableModel(rs));

		mOracleHandler.close();

	}

	private void setRSToTable(ResultSet rs) {
		// TODO Auto-generated method stub
		table.setModel(DbUtils.resultSetToTableModel(rs));
	}

	private int getTotalTeams() {
		mOracleHandler.openDatabase();

		String tableName = DataUtils.TABLE_TOURNAMENT_TEAMS;

		ResultSet rs = null;
		try {
			rs = mOracleHandler.runQuery(SqlHelper.countTable(tableName));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println(tableName + " runQuery failed");
			Tools.pToast(tableName + " runQuery failed");
			return 0;
		}

		int n = Integer.valueOf(RSHelper.getString(rs));
		mOracleHandler.close();
		return n;

	}
}
