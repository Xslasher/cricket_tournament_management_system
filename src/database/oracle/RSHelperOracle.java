package database.oracle;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Team;

public class RSHelperOracle {

	public static String getTableStructure(ResultSet rs) {
		String s = "";
		ResultSetMetaData rsmd = null;

		int totalCol = 0;
		String colNames = "";
		String colTypes = "";

		try {
			rsmd = rs.getMetaData();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			totalCol = rsmd.getColumnCount();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		s = totalCol + "\n";

		for (int i = 0; i < totalCol; i++) {

			try {
				colNames += rsmd.getColumnName(i + 1) + "\t";
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				colTypes += rsmd.getColumnTypeName(i + 1) + "\t";
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		s += colNames + "\n" + colTypes;

		return s;
	}

	public static int getTotalCol(ResultSet rs) {
		// String s = "";
		ResultSetMetaData rsmd = null;

		int totalCol = 0;

		try {
			rsmd = rs.getMetaData();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			totalCol = rsmd.getColumnCount();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return totalCol;
	}

	public static ArrayList<String> getAllRows(ResultSet rs) {
		// String s = "";

		ArrayList<String> al = new ArrayList<>();
		int totalCol = getTotalCol(rs);

		ResultSetMetaData rsmd = null;
		String colType = null;

		try {
			rsmd = rs.getMetaData();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			while (rs.next()) {

				String row = "";
				for (int i = 1; i <= totalCol; i++) {

					colType = rsmd.getColumnTypeName(i);
					String data = "";

					if (colType.equalsIgnoreCase("varchar")
							|| colType.equalsIgnoreCase("varchar2")
							|| colType.equalsIgnoreCase("char")) {
						data = rs.getString(i);
					} else if (colType.equalsIgnoreCase("number")) {
						data = String.valueOf(rs.getInt(i));
					} else if (colType.equalsIgnoreCase("date")) {
						data = rs.getDate(i).toGMTString();
					}
					
					System.out.println(data);

					row += data + "\t";
					// row += rs.getString(i) + "\t" + colType + "\t" ;

				}
				
				System.out.println(row);

				al.add(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return al;
	}
	
	public static ArrayList<Team> getAllTeamBase(ResultSet rs) {
		// String s = "";

		ArrayList<Team> al = new ArrayList<>();
		try {
			while (rs.next()) {
				
				String teamId, teamName, teamCode;
				teamId = rs.getString(1);
				teamName = rs.getString(2);
				teamCode = rs.getString(3);
				Team item = new Team(teamId, teamName, teamCode);
				
				al.add(item);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return al;
	}

}
