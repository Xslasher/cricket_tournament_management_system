package database.oracle;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MainOracle {

	static String USERNAME = "LAB";
	static String PASSWORD = "cse301";
	static String DRIVER_TYPE = "thin";
	static String HOST = "localhost"; // for same computer, Host Name localhost.
	static String PORT = "1521"; // JDBC Port: 1521
	static String SID = "XE"; // for xpress edition;

	// static String URL = "jdbc:oracle:thin:@localhost:1521:XE";
	static String URL = "jdbc:oracle:" + DRIVER_TYPE + ":@" + HOST + ":" + PORT
			+ ":" + SID;

	private static Connection con = null;
	private static Statement stmt = null;
	private static ResultSet rs = null;

	public static void main(String[] argv) {

		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");
			// System.out.println("Oracle JDBC Driver Registered!");

		} catch (ClassNotFoundException e) {

			System.err.println("Database Driver not found");
			e.printStackTrace();
			return;

		}

		try {

			con = DriverManager.getConnection(URL, USERNAME, PASSWORD);
			System.out.println("Database Connected..");
			stmt = con.createStatement();

		} catch (SQLException e) {

			System.err.println("Connection Failed!");
			e.printStackTrace();
			return;

		}

		String sql = "select table_name from user_tables";

		try {
			rs = stmt.executeQuery(sql);

			while (rs.next()) {

				String s = rs.getString(1);
				System.out.println(s);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}