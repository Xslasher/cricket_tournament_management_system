package database.oracle;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class OHandlerOld {

	private static String USERNAME = "LAB";
	private static String PASSWORD = "cse301";
	private static String DRIVER_TYPE = "thin";
	private static String HOST = "localhost"; // for same computer, Host Name
												// localhost.
	private static String PORT = "1521"; // JDBC Port: 1521
	private static String SID = "XE"; // for xpress edition;

	// static String URL = "jdbc:oracle:thin:@localhost:1521:XE";
	private static String URL = "jdbc:oracle:" + DRIVER_TYPE + ":@" + HOST
			+ ":" + PORT + ":" + SID;

	private static Connection con = null;
	private static Statement stmt = null;
	private static ResultSet rs = null;
	private String TABLE_NAME = null;

	public OHandlerOld() {
		// TODO Auto-generated constructor stub
	}

	public OHandlerOld(String username, String pass) {
		// TODO Auto-generated constructor stub
		USERNAME = username;
		PASSWORD = pass;
	}

	public void openDatabase() {

		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");
			// System.out.println("Oracle JDBC Driver Registered!");

		} catch (ClassNotFoundException e) {

			System.err.println("Database Driver not found");
			e.printStackTrace();
			return;

		}

		try {

			con = DriverManager.getConnection(URL, USERNAME, PASSWORD);
			System.out.println("Database Connected..");
			stmt = con.createStatement();

		} catch (SQLException e) {

			System.err.println("Connection Failed!");
			e.printStackTrace();
			return;

		}

	}

	public void readDatabase() {

		String sql = "select table_name from user_tables";

		try {
			rs = stmt.executeQuery(sql);

			while (rs.next()) {

				String s = rs.getString(1);
				System.out.println(s);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ArrayList<String> getUserTables() {

		String sql = "select table_name from user_tables";
		ArrayList<String> al = new ArrayList<String>();

		try {
			rs = stmt.executeQuery(sql);

			while (rs.next()) {

				String s = rs.getString(1);
				// System.out.println(s);
				al.add(s);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return al;

	}

	public int getTotalItem(String tableName) {

		TABLE_NAME = tableName;
		String sql = null;
		int totalItem = 0;

		sql = "SELECT * FROM " + TABLE_NAME;

		try {

			rs = stmt.executeQuery(sql);

			while (rs.next()) {

				totalItem++;

			}
			// System.out.println("Operation get done successfully");

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			// System.exit(0);
			System.err.println(tableName + " Operation count failed");
		}

		return totalItem;
	}

	public void close() {
		try {
			if (rs != null) {
				rs.close();
			}

			if (stmt != null) {
				stmt.close();
			}

			// c.commit();
			if (con != null) {
				con.close();
			}

			System.out.println(USERNAME + " database closed sucessfully");
		} catch (Exception e) {
			System.err.println(USERNAME + " database didn't close sucessfully");
		}

	}

}