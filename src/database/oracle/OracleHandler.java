package database.oracle;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Player;
import utils.DataUtils;
import utils.PrefUtils;
import utils.SqlHelper;

public class OracleHandler {

	private static String USER_NAME;
	private static String PASSWORD;
	private static String DRIVER_TYPE = "thin"; // driver type
	private static String HOST; // for same computer, localhost.
	private static String PORT; // JDBC Port: 1521
	private static String SID; // for xpress edition;

	// static String URL = "jdbc:oracle:thin:@localhost:1521:XE";
	private static String URL = "jdbc:oracle:" + DRIVER_TYPE + ":@" + HOST + ":" + PORT + ":" + SID;

	private static Connection con = null;
	private static Statement stmt = null;
	private static ResultSet rs = null;
	private String TABLE_NAME = null;

	public OracleHandler() {
		// TODO Auto-generated constructor stub
		USER_NAME = PrefUtils.getString(PrefUtils.USER_NAME, DataUtils.DEFAULT_USER_NAME);
		PASSWORD = PrefUtils.getString(PrefUtils.USER_PASS, DataUtils.DEFAULT_USER_PASS);
		HOST = PrefUtils.getString(PrefUtils.HOST, DataUtils.DEFAULT_HOST);
		PORT = PrefUtils.getString(PrefUtils.PORT, DataUtils.DEFAULT_PORT);
		SID = PrefUtils.getString(PrefUtils.SID, DataUtils.DEFAULT_SID);

		URL = "jdbc:oracle:" + DRIVER_TYPE + ":@" + HOST + ":" + PORT + ":" + SID;

	}

	public OracleHandler(String username, String pass) {
		// TODO Auto-generated constructor stub
		// USER_NAME = username;
		// PASSWORD = pass;

		PrefUtils.putString(PrefUtils.USER_NAME, username);
		PrefUtils.putString(PrefUtils.USER_PASS, pass);
		USER_NAME = PrefUtils.getString(PrefUtils.USER_NAME, DataUtils.DEFAULT_USER_NAME);
		PASSWORD = PrefUtils.getString(PrefUtils.USER_PASS, DataUtils.DEFAULT_USER_PASS);
		HOST = PrefUtils.getString(PrefUtils.HOST, DataUtils.DEFAULT_HOST);
		PORT = PrefUtils.getString(PrefUtils.PORT, DataUtils.DEFAULT_PORT);
		SID = PrefUtils.getString(PrefUtils.SID, DataUtils.DEFAULT_SID);

		URL = "jdbc:oracle:" + DRIVER_TYPE + ":@" + HOST + ":" + PORT + ":" + SID;

	}

	public void recheckPreference() {
		// TODO Auto-generated constructor stub
		USER_NAME = PrefUtils.getString(PrefUtils.USER_NAME, DataUtils.DEFAULT_USER_NAME);
		PASSWORD = PrefUtils.getString(PrefUtils.USER_PASS, DataUtils.DEFAULT_USER_PASS);
		HOST = PrefUtils.getString(PrefUtils.HOST, DataUtils.DEFAULT_HOST);
		PORT = PrefUtils.getString(PrefUtils.PORT, DataUtils.DEFAULT_PORT);
		SID = PrefUtils.getString(PrefUtils.SID, DataUtils.DEFAULT_SID);

		URL = "jdbc:oracle:" + DRIVER_TYPE + ":@" + HOST + ":" + PORT + ":" + SID;

	}

	public void openDatabase() {

		recheckPreference();

		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");
			// System.out.println("Oracle JDBC Driver Registered!");

		} catch (ClassNotFoundException e) {

			System.err.println("Database Driver not found");
			e.printStackTrace();
			return;

		}

		try {

			con = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
			System.out.println("Database Connected..");
			stmt = con.createStatement();

		} catch (SQLException e) {

			System.err.println("Connection Failed!");
			e.printStackTrace();
			return;

		}

	}

	// public void runSql(String sql){
	// try {
	// stmt.executeQuery(sql);
	// } catch (SQLException e) {
	// System.err.println(e.getClass().getName() + ": " + e.getMessage());
	// // System.exit(0);
	// System.err.println(" Operation write failed");
	// }
	// }

	public ResultSet runQuery(String sql) throws SQLException {

		// sql = "select table_name from user_tables";
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(sql);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.err.println(sql + " execute failed");
			e.printStackTrace();
			throw new SQLException();
		}
		return rs;

	}

	
	public ResultSet runQuery(String sql, boolean isMoveable) throws SQLException {
		
		// sql = "select table_name from user_tables";
//		PreparedStatement pstmt = con.prepareStatement(sql,ResultSet.TYPE_SCROLL_SENSITIVE, 
//                ResultSet.CONCUR_UPDATABLE);
		
		PreparedStatement pstmt = con.prepareStatement(sql,ResultSet.TYPE_SCROLL_SENSITIVE, 
				ResultSet.CONCUR_READ_ONLY);
		
		
		ResultSet rs = null;
		
		try {
			rs = pstmt.executeQuery();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.err.println(sql + " execute failed");
			e.printStackTrace();
			throw new SQLException();
		}
		return rs;
		
	}
	
	
	public void runUpdate(String sql) throws SQLException {

		// sql = "select table_name from user_tables";
		try {
			stmt.executeUpdate(sql);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.err.println(sql + " execute failed");
			e.printStackTrace();
			throw new SQLException();
		}

	}

	public void runUpdate(String sql, String[] arg) throws SQLException {

		// String sql = "insert into all_cric_teams values (?,?,?)";
		// sql = "insert into all_cric_teams values (?,?,?)";

		PreparedStatement pstmt = con.prepareStatement(sql);
		// pstmt.setString(1, arg[0]);
		// pstmt.setString(2, arg[1]);
		// pstmt.setString(3, arg[2]);

		for (int i = 0; i < arg.length; i++) {
			pstmt.setString(i + 1, arg[i]);
		}

		try {
			pstmt.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.err.println(sql + " execute failed");
			e.printStackTrace();
			throw new SQLException();
		}

	}

	public void insertIntoPlayersTable(Player mPlayer) throws SQLException {

		String sql = SqlHelper.insertValueByPs(DataUtils.TABLE_PLAYERS, 7);

		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setString(1, mPlayer.getPlayerId());
		pstmt.setString(2, mPlayer.getFirstName());
		pstmt.setString(3, mPlayer.getLastName());
		pstmt.setInt(4, mPlayer.getBat());
		pstmt.setInt(5, mPlayer.getBowl());
		pstmt.setInt(6, mPlayer.getWk());
		pstmt.setString(7, mPlayer.getTeamId());
		
		// pstmt.setd

		// for (int i = 0; i < arg.length; i++) {
		// pstmt.setString(i+1, arg[i]);
		// }

		try {
			pstmt.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.err.println(sql + " execute failed");
			e.printStackTrace();
			throw new SQLException();
		}

	}

	public void insertIntoAllCricTeams(String sql, String[] arg) throws SQLException {

		// String sql = "insert into all_cric_teams values (?,?,?)";
		sql = "insert into all_cric_teams values (?,?,?)";

		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setString(1, arg[0]);
		pstmt.setString(2, arg[1]);
		pstmt.setString(3, arg[2]);

		// for (int i = 0; i < arg.length; i++) {
		// pstmt.setString(i+1, arg[i]);
		// }

		try {
			pstmt.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.err.println(sql + " execute failed");
			e.printStackTrace();
			throw new SQLException();
		}

	}

	public void runSql(String sql) throws SQLException {

		// sql = "select table_name from user_tables";
		// boolean b;
		try {
			// b = stmt.execute(sql);
			stmt.execute(sql);
			// return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.err.println(sql + " execute failed");
			e.printStackTrace();
			throw new SQLException();
			// return false;

		}

	}

	public void insertValue(String sql) throws SQLException {

		// String sql = "insert into all_cric_teams values (?,?,?)";
		// sql = "insert into all_cric_teams (team_id,team_name, team_code)
		// values ('ii','ii','ii')";

		// sql = "insert into all_cric_teams values ('" + arr[0] + ',' + arr[1]
		// + ',' + arr[2] + "')";

		try {
			stmt.executeUpdate(sql);
			con.commit();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.err.println(sql + " execute failed");
			e.printStackTrace();
			throw new SQLException();
		}

	}

	public void insertDataByArg(String tableName, String col_1_data, String col_2_data, String col_3_data) {

		TABLE_NAME = tableName;
		String sql = null;

		String COL_1_TITLE = "team_id";
		String COL_2_TITLE = "team_name";
		String COL_3_TITLE = "team_title";
		// String COL_3_TITLE = null;

		String arg = COL_1_TITLE + "," + COL_2_TITLE + "," + COL_3_TITLE;

		String value = col_1_data + "','" + col_2_data + "'," + col_3_data;

		//
		// String arg = COL_0_TITLE + "," + COL_1_TITLE + "," + COL_2_TITLE +
		// ","
		// + COL_3_TITLE;
		//
		// String value = id + ",'" + col_1_data + "','" + col_2_data + "',"
		// + col_3_data;
		//
		//

		sql = "INSERT INTO " + TABLE_NAME + " (" + arg + ") " + "VALUES (" + value + ");";
		try {

			stmt.executeUpdate(sql);
			con.commit();

			System.out.println("data insertion successfull");

		} catch (Exception e) {
			System.err.println("data insertion failed");
			System.err.println("----------------------");
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			// System.err.println(id + " : " + col_1_data);
		}

	}

	public ArrayList<String> getUserTables() {

		String sql = "select table_name from user_tables";
		ArrayList<String> al = new ArrayList<String>();

		try {
			rs = stmt.executeQuery(sql);

			while (rs.next()) {

				String s = rs.getString(1);
				// System.out.println(s);
				al.add(s);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return al;

	}

	public boolean isTableExist(String tableName) {

		tableName = tableName.toUpperCase();
		String sql = "select count(*) as c from user_tables where table_name = '" + tableName + "'";
		System.out.println(sql);
		boolean b = false;
		try {
			rs = stmt.executeQuery(sql);
			if (rs.next()) {

				if (rs.getInt("c") > 0) {
					b = true;
				}
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.err.println("isTableExists func failed");
			e.printStackTrace();
		}

		return b;

	}

	public int getTotalItem(String tableName) {

		TABLE_NAME = tableName;
		String sql = null;
		int totalItem = 0;

		sql = "SELECT * FROM " + TABLE_NAME;

		try {

			rs = stmt.executeQuery(sql);

			while (rs.next()) {

				totalItem++;

			}
			// System.out.println("Operation get done successfully");

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			// System.exit(0);
			System.err.println(tableName + " Operation count failed");
		}

		return totalItem;
	}

	public void close() {
		try {
			if (rs != null) {
				rs.close();
			}

			if (stmt != null) {
				stmt.close();
			}

			// con.commit(); //in experiment
			if (con != null) {
				con.commit(); // in experiment
				con.close();
			}

			System.out.println(USER_NAME + " database closed sucessfully");
		} catch (Exception e) {
			System.err.println(USER_NAME + " database didn't close sucessfully");
			//e.printStackTrace();
		}

	}

}