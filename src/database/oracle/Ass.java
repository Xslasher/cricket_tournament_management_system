/*
 * Ass No	: 03
 * Id 		: 2013-1-60-052
 * Name		: Nasif Ahmed
 * Status	: Ok
 * 
 * */

package database.oracle;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JTextArea;

public class Ass {

//	static String USERNAME = "LAB";		//default
//	static String PASSWORD = "cse301";	//default

	static String USERNAME;		//default
	static String PASSWORD;		//default

	static String URL = "jdbc:oracle:thin:@localhost:1521:XE";
	
	private static Connection con = null;
	private static Statement stmt = null;
	private static ResultSet rs = null;

	public static void main(String[] argv) {

		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");
			System.out.println("Oracle JDBC Driver Registered!");
			printMessage("Oracle JDBC Driver Registered!", "Driver registration");
			
			

		} catch (ClassNotFoundException e) {

			System.err.println("Database Driver not found");
			printMessage("Database Driver not found", "Driver registration");
			e.printStackTrace();
			return;

		}
		
		USERNAME = getStringFromInputBox("Enter Username");
		PASSWORD = getStringFromInputBox("Enter Password");

		try {

			con = DriverManager.getConnection(URL, USERNAME, PASSWORD);
			System.out.println("Database Connected..");
			printMessage("Database Connected..", "Database Connection");
			stmt = con.createStatement();

		} catch (SQLException e) {

			System.err.println("Connection Failed!");
			printMessage("Database Connection Failed", "Database Connection");
			e.printStackTrace();
			return;

		}

		String sql = getStringFromInputBox("Enter a Query");
		String result = getResult(sql);
		
		printMessage2(result, "Result");

		

	}
	
	private static void printMessage(String msg, String title){
		JOptionPane.showMessageDialog(null,
			    msg,
			    title,
			    JOptionPane.PLAIN_MESSAGE);
	}
	
	private static void printMessage2(String msg, String title){
		JOptionPane.showMessageDialog(null,
			    new JTextArea(msg),
			    title,
			    JOptionPane.PLAIN_MESSAGE);
	}
	
	
	private static String getStringFromInputBox(String title){
		String s=JOptionPane.showInputDialog(null, title);
		return s;
	}
	
	private static String getResult(String sql){
		 
		//sql = "select table_name from user_tables";
		//sql = "select name from instructor";
		String result = ""; 
		
		sql = sql.replace(';', ' ');
		
		//int tCol;
		
		try {
			rs = stmt.executeQuery(sql);
			//tCol = getTotalCol(rs);

//			while (rs.next()) {
//
//				String s = rs.getString(1);
//				System.out.println(s);
//				result += s+"\n";
//
//			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ArrayList<String> al = getAllRows(rs);
		
		for (int i = 0; i < al.size(); i++) {
			result += al.get(i)+"\n";
		}
		
		
		
		return result;
	}
	
	public static int getTotalCol(ResultSet rs) {
		// String s = "";
		ResultSetMetaData rsmd = null;

		int totalCol = 0;

		try {
			rsmd = rs.getMetaData();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			totalCol = rsmd.getColumnCount();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return totalCol;
	}
	
	public static ArrayList<String> getAllRows(ResultSet rs) {
		// String s = "";

		ArrayList<String> al = new ArrayList<>();
		int totalCol = getTotalCol(rs);

		ResultSetMetaData rsmd = null;
		String colType = null;

		try {
			rsmd = rs.getMetaData();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			while (rs.next()) {

				String row = "";
				for (int i = 1; i <= totalCol; i++) {

					colType = rsmd.getColumnTypeName(i);
					String data = "";

					if (colType.equalsIgnoreCase("varchar")
							|| colType.equalsIgnoreCase("varchar2")
							|| colType.equalsIgnoreCase("char")) {
						data = rs.getString(i);
					} else if (colType.equalsIgnoreCase("number")) {
						data = String.valueOf(rs.getInt(i));
					} else if (colType.equalsIgnoreCase("date")) {
						data = rs.getDate(i).toGMTString();
					}
					
					//System.out.println(data);

					row += data + "\t";
					// row += rs.getString(i) + "\t" + colType + "\t" ;

				}
				
				System.out.println(row);

				al.add(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return al;
	}

}
