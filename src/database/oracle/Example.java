package database.oracle;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import utils.Tools;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

public class Example extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Example frame = new Example();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Example() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnSadhin = new JButton("sadhin");
		btnSadhin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Tools.printMessage("sdhing is boss");
			}
		});
		btnSadhin.setBounds(105, 36, 89, 23);
		contentPane.add(btnSadhin);
		
		textField = new JTextField();
		textField.setBounds(105, 79, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
	}

}
