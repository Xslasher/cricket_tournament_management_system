package utils;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class PdfHelper {

	public static void createPdfFromResultSet (ResultSet rs, String fileName, String reportTitle) throws Exception{

		Document mDocument = new Document();
		try {
			PdfWriter.getInstance(mDocument, new FileOutputStream(fileName));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mDocument.open();

		ResultSetMetaData rsmd = null;
		int columnsNumber = 0;
		try {
			rsmd = rs.getMetaData();
			columnsNumber = rsmd.getColumnCount();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// we have four columns in our table
		PdfPTable mPdfPTable = new PdfPTable(columnsNumber);
		// create a cell object
		PdfPCell cell;

		for (int i = 1; i <= columnsNumber; i++) {
			String str = null;
			try {
				str = rsmd.getColumnName(i);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			cell = new PdfPCell(new Phrase(str));
			mPdfPTable.addCell(cell);

		}

		try {
			while (rs.next()) {
				for (int i = 1; i <= columnsNumber; i++) {
					String str = rs.getString(i);
					cell = new PdfPCell(new Phrase(str));
					mPdfPTable.addCell(cell);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		Paragraph parReportTitle = new Paragraph();
		parReportTitle.add(reportTitle+"\n\n\n");
		parReportTitle.setAlignment(Element.ALIGN_CENTER);

		//String timestamp = new Timestamp(System.currentTimeMillis()).toString();
		String timestamp = TimeHelper.getCurrentTimeDate();
		Paragraph parReportDate = new Paragraph();
		parReportDate.add("\n\n\n Report generated on " + timestamp);
		parReportDate.setAlignment(Element.ALIGN_CENTER);

		try {

			if (reportTitle != null) {
				// Attach report title to PDF
				mDocument.add(parReportTitle);
			}

			// Attach report table to PDF
			mDocument.add(mPdfPTable);

			// Attach report date PDF
			mDocument.add(parReportDate);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		mDocument.addCreationDate();

		mDocument.close();

		/* Close all DB related objects */
//		try {
//			rs.close();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}

	}

}
