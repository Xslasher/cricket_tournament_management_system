package utils;


import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;


public class ImageUtils {

	public static URL getImageUrl(String imageName) {

		//make sure image name with extension
		URL url = Context.class.getResource("/" + imageName);
		return url;

	}

	public static ImageIcon getImageIcon(String imageName) {

		URL url = Context.class.getResource("/" + imageName);
		ImageIcon ic = new ImageIcon(url);
		return ic;
	}

	public static JButton getImageButton(String imageName) {

		URL url = Context.class.getResource("/" + imageName);
		ImageIcon ic = new ImageIcon(url);
		JButton bt = new JButton(ic);
		return bt;
	}

}
