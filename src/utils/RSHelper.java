package utils;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import model.PlayerB;
import model.Match;
import model.Player;
import model.Team;
import model.TournamentTeam;

public class RSHelper {
	
	public static String getString(ResultSet rs){
		
		if(rs==null){
			return null;
		}
		
		String s = null;
		
		try {
			if(rs.next()){
				s = rs.getString(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return s;
		
	}

	public static Match getAMatch(ResultSet rs, String matchType){
		
		if(rs==null){
			return null;
		}
		
		Match match = null;
		if(matchType.equalsIgnoreCase(DataUtils.MATCH_ODI) || matchType.equalsIgnoreCase(DataUtils.MATCH_T20)){
			
			int matchNo = 0;
			double over = 0;
			String matchId = null, team1 = null, team2 = null,date = null;
			
			try {
				if(rs.next()){
					matchNo = rs.getInt("match_no");
					matchId = rs.getString("match_id");
					team1 = rs.getString("team1");
					team2 = rs.getString("team2");
					date = rs.getString("play_date");
					over = Double.parseDouble(rs.getString("over"));
				}
			} catch (NumberFormatException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.err.println("Get a match failed");
				e.printStackTrace();
			}
		
			
			match = new Match(matchId, matchNo, team1, team2, date, over, matchType);
			
		}
		return match;
	}
	
	public static ArrayList<Team> getAllTeamBase(ResultSet rs) {
		// String s = "";

		ArrayList<Team> al = new ArrayList<>();
		try {
			while (rs.next()) {

				String teamId, teamName, teamCode;
				teamId = rs.getString(1);
				teamName = rs.getString(2);
				teamCode = rs.getString(3);
				Team item = new Team(teamId, teamName, teamCode);

				al.add(item);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return al;
	}

	public static ArrayList<TournamentTeam> getTournamentTeams(ResultSet rs) {
		// String s = "";

		ArrayList<TournamentTeam> al = new ArrayList<>();
		try {
			while (rs.next()) {

				String teamId, teamName, teamCode, captain, viceCaptain, coach, manager, playersTable;
				// teamId = rs.getString(1);
				// teamName = rs.getString(2);
				// teamCode = rs.getString(3);
				// Team item = new Team(teamId, teamName, teamCode);
				//
				// al.add(item);

				teamId = rs.getString("team_id");
				teamName = rs.getString("team_name");
				teamCode = rs.getString("team_code");
				captain = rs.getString("captain");
				viceCaptain = rs.getString("vice_cap");
				coach = rs.getString("coach");
				manager = rs.getString("manager");
				

				// TournamentTeam item = new TournamentTeam();
				// item.setTeamId(teamId);
				// item.setTeamName(teamName);
				// item.setTeamCode(teamCode);

				TournamentTeam item = new TournamentTeam(teamId, teamName, teamCode, captain, viceCaptain, coach,
						manager,null);

				al.add(item);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return al;
	}

	public static ArrayList<PlayerB> getSquadB(ResultSet rs) {
		// String s = "";

		ArrayList<PlayerB> al = new ArrayList<>();

		String playerId;
		String firstName;
		String lastName;
		boolean bat, bowl, wk;
		String teamId = null;

		try {
			while (rs.next()) {

				playerId = rs.getString("player_id");
				firstName = rs.getString("first_name");
				lastName = rs.getString("last_name");
				bat = rs.getBoolean("bat");
				bowl = rs.getBoolean("bowl");
				wk = rs.getBoolean("wk");
				// teamId = rs.getString("team_id");

				PlayerB item = new PlayerB(playerId, firstName, lastName, bat, bowl, wk, teamId);

				al.add(item);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return al;
	}

	public static ArrayList<Player> getSquad(ResultSet rs) {
		// String s = "";

		ArrayList<Player> al = new ArrayList<>();

		String playerId;
		String firstName;
		String lastName;
		int bat, bowl, wk;
		String teamId = null;

		try {
			while (rs.next()) {

				playerId = rs.getString("player_id");
				firstName = rs.getString("first_name");
				lastName = rs.getString("last_name");
				bat = rs.getInt("bat");
				bowl = rs.getInt("bowl");
				wk = rs.getInt("wk");
				// teamId = rs.getString("team_id");

				Player item = new Player(playerId, firstName, lastName, bat, bowl, wk, teamId);

				al.add(item);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return al;
	}

	public static ArrayList<String> getStringArrayList(ResultSet rs) {
		// String s = "";

		ArrayList<String> al = new ArrayList<>();
		try {
			while (rs.next()) {

				String s;
				s = rs.getString(1);

				al.add(s);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return al;
	}

	public static String getTableStructure(ResultSet rs) {
		String s = "";
		ResultSetMetaData rsmd = null;

		int totalCol = 0;
		String colNames = "";
		String colTypes = "";

		try {
			rsmd = rs.getMetaData();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			totalCol = rsmd.getColumnCount();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		s = totalCol + "\n";

		for (int i = 0; i < totalCol; i++) {

			try {
				colNames += rsmd.getColumnName(i + 1) + "\t";
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				colTypes += rsmd.getColumnTypeName(i + 1) + "\t";
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		s += colNames + "\n" + colTypes;

		return s;
	}

	public static int getTotalCol(ResultSet rs) {
		// String s = "";
		ResultSetMetaData rsmd = null;
		int totalCol = 0;
		try {
			totalCol = rs.getMetaData().getColumnCount();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return totalCol;
	}

	public static int getTotalResult(ResultSet rs) {

		int totalItem = 0;

		try {
			while (rs.next()) {

				totalItem++;

			}
		} catch (Exception e) {
			// TODO: handle exception

			totalItem = 0;
		}

		return totalItem;
	}

}
