package utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class TimeHelper {

	private static String timeVal(int n) {

		String s;
		if (n < 10) {
			s = "0" + n;
		} else {
			s = "" + n;
		}
		return s;
	}

	public static String getCurrentTimeDate() {

		Calendar cal = new GregorianCalendar();

		int day = cal.get(Calendar.DAY_OF_MONTH);
		int month = cal.get(Calendar.MONTH);
		int year = cal.get(Calendar.YEAR);
		int amPm = cal.get(Calendar.AM_PM);

		int second = cal.get(Calendar.SECOND);
		int min = cal.get(Calendar.MINUTE);
		int hour = cal.get(Calendar.HOUR);

		String time = "", date = "";

		// time= hour+":"+min+":"+second;
		// date = day+"/"+month+"/"+year;

		time = timeVal(hour) + ":" + timeVal(min) + ":" + timeVal(second);
		
		if(amPm==Calendar.AM){
			time = time + " AM";
		}else{
			time = time + " PM";
		}
		
		
		date = timeVal(day) + "/" + timeVal(month) + "/" + timeVal(year);

		String s = time + " " + date;

		return s;

	}

	public static String getLocalTimeFromUTC(String inputDate) {

		// utc = "2015-01-13 18:30:00";
		String outputDate;

		SimpleDateFormat dfInput;
		// dfInput = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dfInput = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		dfInput.setTimeZone(TimeZone.getTimeZone("GMT"));

		// http://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html
		SimpleDateFormat dfOutput;
		// dfOutput = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// dfOutput = new SimpleDateFormat("MM-dd HH:mm");
		// dfOutput = new SimpleDateFormat("KK:mm a | dd"); ////kk for am pm
		// converting
		dfOutput = new SimpleDateFormat("hh:mm a | dd"); //// HH for am pm
															//// converting

		// sdfmad.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
		dfOutput.setTimeZone(TimeZone.getDefault());

		Date mDate = null;
		try {
			mDate = dfInput.parse(inputDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		// System.out.println("GMT:\t\t" + sdfgmt.format(inptdate));
		// System.out.println("Europe/Madrid:\t" + sdfmad.format(inptdate));

		outputDate = dfOutput.format(mDate);

		return outputDate;

	}

	public static String getDate(String inputDate) {

		String outputDate = null;
		SimpleDateFormat dfInput = null, dfOutput;

		if (inputDate.contains("-")) {
			dfInput = new SimpleDateFormat("dd-MM-yyyy");
		} else if (inputDate.contains("/")) {
			dfInput = new SimpleDateFormat("dd/MM/yyyy");
		}

		dfOutput = new SimpleDateFormat("dd-MMM-yyyy");

		Date mDate = null;
		try {
			mDate = dfInput.parse(inputDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		outputDate = dfOutput.format(mDate);

		return outputDate;
	}

	public static String makeFullTime(String date, String time, int dayVar) {
		// yyyy-MM-dd HH:mm:ss
		// dd-MMM-yyyy HH:mm:ss
		String fullTime = null;

		// date="14-Feb";
		// time="01:22 UTC";

		if (dayVar == -1) {
			String[] s = date.split("-");
			int day = Integer.valueOf(s[0]);
			String mon = s[1];
			day = day + dayVar;

			date = day + "-" + mon + "-2015";

		} else {
			date = date + "-2015";
		}

		time = time.substring(0, 5) + ":00";

		fullTime = date + " " + time;

		// System.out.println(fullTime);
		return fullTime;

	}

	public static String[] get2DigitRep(String s) {

		String[] parts = s.split(":");

		// String day=parts[0];
		// String hour=parts[1];
		// String min=parts[2];
		// String sec=parts[3];

		for (int i = 0; i < parts.length; i++) {

			if (parts[i].length() == 1) {
				parts[i] = "0" + parts[i];
			}
		}

		return parts;

	}

	public static long getCurrentUTCinMilli() {

		Calendar aGMTCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

		// System.out.println(aGMTCalendar.toString());
		// long mill = aGMTCalendar.getTimeInMillis();
		long mill = System.currentTimeMillis();
		// System.out.println(mill);

		// Date date = new Date(mill);
		// System.out.println(date.toGMTString());

		return mill;
	}

	public static long get24FebUTCinMilli() {

		// 14-feb 2015 22:00:00 UTC final
		// 1423951200000 utc in mili final
		// long mill = 1423951200000L;
		long mill = 1423864800000L;

		return mill;
	}

	public static String getTotalDayFromMill(long mill) {

		long timeInMilliSeconds = mill;

		long seconds = timeInMilliSeconds / 1000;
		long minutes = seconds / 60;
		long hours = minutes / 60;
		long days = hours / 24;
		String time = days + ":" + hours % 24 + ":" + minutes % 60 + ":" + seconds % 60;
		return time;
		// System.out.println(date.toString());
		// return date.toGMTString();

		// Date date=new Date(mill);
		// SimpleDateFormat dfOutput;
		// // dfOutput = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// // dfOutput = new SimpleDateFormat("MM-dd HH:mm");
		// //dfOutput = new SimpleDateFormat("KK:mm a | dd"); ////kk for am pm
		// converting
		// dfOutput = new SimpleDateFormat("dd:hh:mm:ss"); ////HH for am pm
		// converting
		// dfOutput.setTimeZone(TimeZone.getTimeZone("GMT"));
		// return dfOutput.format(date);
	}

}
