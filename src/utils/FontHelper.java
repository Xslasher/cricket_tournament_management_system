package utils;

import java.awt.Font;

public class FontHelper {

	public static int FONT_SIZE_SMALL = 9;
	public static int FONT_SIZE_NORMAL = 14;
	public static int FONT_SIZE_BIG = 30;

	public static String FONT_NAME_DEFAULT = "Serif";

	public static Font getNormalFont() {

		Font font = null;
		font = new Font(FONT_NAME_DEFAULT, Font.PLAIN, FONT_SIZE_NORMAL);
		return font;

	}

	public static Font getBoldFont() {

		Font font = null;
		font = new Font(FONT_NAME_DEFAULT, Font.BOLD, FONT_SIZE_NORMAL);
		return font;

	}

	public static Font getItalicFont() {

		Font font = null;
		font = new Font(FONT_NAME_DEFAULT, Font.ITALIC, FONT_SIZE_NORMAL);
		return font;

	}

	public static Font getBoldItalicFont() {

		Font font = null;
		font = new Font(FONT_NAME_DEFAULT, Font.BOLD + Font.ITALIC,
				FONT_SIZE_NORMAL);
		return font;

	}

	public static Font getBigFont() {

		Font font = null;
		font = new Font(FONT_NAME_DEFAULT, Font.BOLD + Font.ITALIC,
				FONT_SIZE_BIG);
		return font;

	}

	public static Font getSmallFont() {

		Font font = null;
		font = new Font(FONT_NAME_DEFAULT, Font.BOLD + Font.ITALIC,
				FONT_SIZE_SMALL);
		return font;

	}

}
